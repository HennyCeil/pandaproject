﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class HelpersController : MonoBehaviour {
	public static HelpersController instance;
	public HelperClass[] Helpers;
	// Use this for initialization
	private double TotalHelpersDPS;
	public Text[] helperDPStexts;
	LanguageManager LM;

	void Awake(){
		instance = this;
	}
	void Start () {
		LM = SmartLocalization.LanguageManager.Instance;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		TotalHelpersDPS = 0;
		foreach (HelperClass help in Helpers){
			if(help.Unlocked)
				TotalHelpersDPS+=help.currentDPS;
		}
		foreach (Text hdps in helperDPStexts){
			hdps.text = NumberToString.instance.Convert(TotalHelpersDPS) + " " + LM.GetTextValue("Game.DPSabb");
		}

	}
	public double GetTotalDPS(){
		return TotalHelpersDPS;
	}
	public double GetTapDamageBonus(){
		double percentage = 1;
		double neatdamage = 0;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.DamageTap && help.SkillIsActive[i])
					percentage+=help.SkillValue[i];
				if(help.Skills[i] == HelperClass.HelperSkillType.DamageTapFromDPS && help.SkillIsActive[i])
					neatdamage+=help.SkillValue[i]*GetTotalDPS();
			}
		}
		return neatdamage + GameManager.instance.damage*percentage;
	}
	public double GetHelpersDamageBonus(){
		return System.Math.Floor(0.1d * GetTotalDPS());
	}
	public float GetCritChanceBonus(){
		float critneat = 0;
		float critoncd = 0;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.CritChance && help.SkillIsActive[i])
					critneat+=(float)help.SkillValue[i];
				if(help.Skills[i] == HelperClass.HelperSkillType.CritChanceOnCD && help.SkillIsActive[i])
					critoncd+=(float)help.SkillValue[i];/*GetIfCD())*/
			}
		}
		//if(LoadAllLanguages on cd)
		//	return critneat + critoncd;
		//else
			return critneat;
	}
	public double GetCritDamageBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.CritDamage && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetCritDamageBossBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.CritDamageBoss && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetDamageOnBoss(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.DamageBoss && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetDamageAllBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.DamageAll && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetGoldAllBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.GoldAll && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetGoldBossBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.GoldBoss && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetGoldCageBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.GoldCage && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
	public double GetGoldMonstereBonus(){
		double value = 1;
		foreach (HelperClass help in Helpers){
			for(int i=0; i<help.Skills.Length; i++){
				if(help.Skills[i] == HelperClass.HelperSkillType.GoldMonster && help.SkillIsActive[i])
					value+=help.SkillValue[i];
			}
		}
		return  value;
	}
}
