﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelperUIPasiveSkillController : MonoBehaviour {
	public Text SkillName;

	// Use this for initialization
	public void SetSkillName (string n, bool active) {
		SkillName.text = n;
		if(active)
			SkillName.color = Color.white;
		else
			SkillName.color = Color.gray;
	}
}
