﻿using UnityEngine;
using System.Collections;

public class CoinSystemController : MonoBehaviour {

	public static CoinSystemController instance;
	public CoinController[] Coins;
	private int current = 0;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		Coins = new CoinController[transform.childCount];
		for(int i = 0; i < Coins.Length; i++){
			Coins[i] = transform.GetChild(i).GetComponent<CoinController>();
		}
	}
	
	public void InstantiateCoin(double value, Vector3 pos){
		Coins[current].value = value;
		Coins[current].pos = pos;
		Coins[current].gameObject.SetActive(true);
		current++;
		if(current == Coins.Length)
			current = 0;
	}
	public void InstantiateCoin(double value){
		Coins[current].value = value;
		Coins[current].gameObject.SetActive(true);
		current++;
		if(current == Coins.Length)
			current = 0;
	}
	public void SplitInCoins(double gld, Vector3 pos, double split = 5.0d){
		if(gld<1)
			gld = 1;
		if(split == 1){
			InstantiateCoin(gld, pos);
			return;
		}
		if(split>6){
			double di = gld/split;
			while(gld>=di){
				InstantiateCoin(System.Math.Floor(di), pos);
				gld-=di;
			}
			if(gld>1d)
				InstantiateCoin(System.Math.Floor(gld), pos);
			return;
		}
		if(gld==1d){
			InstantiateCoin(1d, pos);
		}else if(gld<5d){
			for(int i=0; i<gld; i++)
				InstantiateCoin(1d, pos);
		}else{
			if(gld<10d){
				while(gld>2d){
					InstantiateCoin(2d, pos);
					gld-=2d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld), pos);
			}else if(gld<=50d){
				while(gld>10d){
					InstantiateCoin(10d, pos);
					gld-=10d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld),pos);
			}else if(gld<=100d){
				while(gld>20d){
					InstantiateCoin(20d, pos);
					gld-=20d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld),pos);
			}else if(gld<=500d){
				while(gld>100d){
					InstantiateCoin(100d, pos);
					gld-=100d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld),pos);
			}else if(gld<=1000d){
				while(gld>150d){
					InstantiateCoin(150d, pos);
					gld-=150d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld),pos);
			}else if(gld>1000d){
				double di = gld/split;
				while(gld>=di){
					InstantiateCoin(System.Math.Floor(di), pos);
					gld-=di;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld), pos);
			}
		}
	}
	public void SplitInCoins(double gld, double split = 5.0d){
		if(gld<1)
			gld = 1;
		if(split == 1){
			InstantiateCoin(gld);
			return;
		}
		if(split>6){
			double di = gld/split;
			while(gld>=di){
				InstantiateCoin(System.Math.Floor(di));
				gld-=di;
			}
			if(gld>1d)
				InstantiateCoin(System.Math.Floor(gld));
			return;
		}
		if(gld==1d){
			InstantiateCoin(1d);
		}else if(gld<5d){
			for(int i=0; i<gld; i++)
				InstantiateCoin(1d);
		}else{
			if(gld<10d){
				while(gld>2d){
					InstantiateCoin(2d);
					gld-=2d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}else if(gld<=50d){
				while(gld>10d){
					InstantiateCoin(10d);
					gld-=10d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}else if(gld<=100d){
				while(gld>20d){
					InstantiateCoin(20d);
					gld-=20d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}else if(gld<=500d){
				while(gld>100d){
					InstantiateCoin(100d);
					gld-=100d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}else if(gld<=1000d){
				while(gld>150d){
					InstantiateCoin(150d);
					gld-=150d;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}else if(gld>1000d){
				double di = gld/split;
				while(gld>=di){
					InstantiateCoin(System.Math.Floor(di));
					gld-=di;
				}
				if(gld>1d)
					InstantiateCoin(System.Math.Floor(gld));
			}
		}
	}
}
