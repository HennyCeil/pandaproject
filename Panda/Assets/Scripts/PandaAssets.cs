﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class PandaAssets : IStoreAssets{
	

	public int GetVersion() {
		return 0;
	}
	
	// NOTE: Even if you have no use in one of these functions, you still need to
	// implement them all and just return an empty array.
	
	public VirtualCurrency[] GetCurrencies() {
		return new VirtualCurrency[]{MAGIC_BEAN};
	}
	
	public VirtualGood[] GetGoods() {
		return new VirtualGood[] {/*SHIELD_GOOD,*/ NO_ADS_LTVG};
	}
	
	public VirtualCurrencyPack[] GetCurrencyPacks() {
		return new VirtualCurrencyPack[] {MAGIC_BEANS_PACK1, MAGIC_BEANS_PACK2,MAGIC_BEANS_PACK3,MAGIC_BEANS_PACK4,MAGIC_BEANS_PACK5, MAGIC_BEANS_PACK6};
	}
	
	public VirtualCategory[] GetCategories() {
		return new VirtualCategory[]{/*GENERAL_CATEGORY*/};
	}
	
	/** Virtual Currencies **/
	
	public static VirtualCurrency MAGIC_BEAN = new VirtualCurrency(
		"Magic Beab",                               // Name
		"Powerful Magic Bean",                      // Description
		"magic_bean_ID"                    // Item ID
		);
	
	/** Virtual Currency Packs **/
	
	public static VirtualCurrencyPack MAGIC_BEANS_PACK1 = new VirtualCurrencyPack(
		"200 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack1_ID",                       // Item ID
		200,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack1",                   // Product ID
	                       1.99                                   // Price (in real money $)
	                       )
		);
	public static VirtualCurrencyPack MAGIC_BEANS_PACK2 = new VirtualCurrencyPack(
		"580 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack2_ID",                       // Item ID
		580,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack2",                   // Product ID
	                       4.99                                   // Price (in real money $)
	                       )
		);
	public static VirtualCurrencyPack MAGIC_BEANS_PACK3 = new VirtualCurrencyPack(
		"1500 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack3_ID",                       // Item ID
		1500,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack3",                   // Product ID
	                       9.99                                   // Price (in real money $)
	                       )
		);
	public static VirtualCurrencyPack MAGIC_BEANS_PACK4 = new VirtualCurrencyPack(
		"3800 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack4_ID",                       // Item ID
		3800,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack4",                   // Product ID
	                       24.99                                   // Price (in real money $)
	                       )
		);
	public static VirtualCurrencyPack MAGIC_BEANS_PACK5 = new VirtualCurrencyPack(
		"8500 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack5_ID",                       // Item ID
		8500,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack5",                   // Product ID
	                       49.99                                   // Price (in real money $)
	                       )
		);
	public static VirtualCurrencyPack MAGIC_BEANS_PACK6 = new VirtualCurrencyPack(
		"20000 Magic Beans",                          // Name
		"Ads Optional",            // Description
		"magic_bean_pack6_ID",                       // Item ID
		20000,                                  // Number of currencies in the pack
		"magic_bean_ID",                   // ID of the currency associated with this pack
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "com.juiceglobal.magicbeanspack6",                   // Product ID
	                       99.99                                   // Price (in real money $)
	                       )
		);
	
	/** Virtual Goods **/
	
	/*public static VirtualGood SHIELD_GOOD = new SingleUseVG(
		"Shield",                             // Name
		"Protect yourself from enemies",      // Description
		"shield_ID",                          // Item ID
		new PurchaseWithVirtualItem(          // Purchase type (with virtual currency)
	                            "coin_currency_ID",                     // ID of the item used to pay with
	                            225                                    // Price (amount of coins)
	                            )
		);*/
	
	// NOTE: Create non-consumable items using LifeTimeVG with PurchaseType of PurchaseWithMarket.
	public static VirtualGood NO_ADS_LTVG = new LifetimeVG(
		"No Ads",                             // Name
		"No More Ads!",                       // Description
		"no_ads_ID",                          // Item ID
		new PurchaseWithMarket(               // Purchase type (with real money $)
	                       "no_ads_PROD_ID",                      // Product ID
	                       0.99                                   // Price (in real money $)
	                       )
		);
	
	/** Virtual Categories **/
	/*
	public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
		"General", new List<string>(new string[] {SHIELD_GOOD})
		);*/
	
}