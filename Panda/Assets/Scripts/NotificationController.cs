﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotificationController : MonoBehaviour {
	public static NotificationController instance;
	public Text texto;
	public Image img;
	public GameObject notpanel;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	public void Show(){
		notpanel.SetActive(true);
	}
	public void Hide(){
		CancelInvoke("Hide");
		notpanel.SetActive(false);
	}
	// Update is called once per frame
	public void SetNotification (string t, Sprite s, float duration) {
		texto.text = t;
		if(s!=null)
			img.sprite = s;
		else
			img.enabled = false;
		Invoke("Hide", duration);
	}
}
