﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class SettingsManager : MonoBehaviour {
	public static SettingsManager instance;
	public GameObject Panel;
	public GameObject ActionPanel;
	LanguageManager LM;
	int currlang = 0;
	List<SmartCultureInfo> SupportedLanguages;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		LM = SmartLocalization.LanguageManager.Instance;
		SupportedLanguages = LM.GetSupportedLanguages();
		foreach(SmartCultureInfo info in SupportedLanguages)
			Debug.Log (info.englishName);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Hide(){
		ActionPanel.SetActive (true);
		Panel.SetActive(false);
	}

	public void Show(){
		ActionPanel.SetActive (false);
		Panel.SetActive(true);
	}
	public void RotateLanguage(){
		currlang++;
		if(currlang>=SupportedLanguages.Count)
			currlang = 0;
		LM.ChangeLanguage(SupportedLanguages[currlang]);

	}
}
