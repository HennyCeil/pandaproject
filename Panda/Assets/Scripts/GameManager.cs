﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {
	public static GameManager instance;
	public Image enemycounter;
	public Animator Enemy;
	public Image monsterhpbar;
	public Image monstertimebar;
	public Text monsterhptext;
	public Text monstertimetext;
	public Text goldtext;
	public Text goldtext2;
	public Text goldtext3;
	public Text charleveltext;
	public Text nextdamageprice;
	public Text nextdamage;
	public Text tapdamagetext;
	public Text tapdamagetext2;
	public Text tapdamagetext3;
	public Text BossButtonText;
	public Text MonsterLevel;
	public Text MonsterCounter;
	public Text DPSText;
	public Image PandaIncrementer;
	public GameObject BossButton;
	public GameObject BossTimer;
	public Animator CamShaker;
	public Animator Fader;
	public Animator Transitioner;
	public Text TransText;
	public Color activecolor;
	public Sprite Pawtomatic;

	public Object HitParticle;

	public float secondsToKillBoss;
	private float secondsLeftToKillBoss;
	public double damage;

	private double damagecost;
	public int level = 1;
	private float critchance = 100;
	private float critdamage = 1f;

	private double dps=0;
	private double critd;
	private Vector3 touchPosition;
	private Touch touch;
	private double g;
	private double acum = 0;
	private double nextd = 1;
	private bool isautotap = false;
	private bool isholding = false;
	private MonstersController MonsterC;

	public GameObject Tucan;
	public Animator Shooter;
	public Animator PandaAnim;
	public ParticleSystem fireshoot;
	public GameObject humito;
	public Transform humitopos;
	private double currdamage = 0;

	public GameObject plus10;
	public GameObject plus100;
	
	private bool plus10active = false;
	private bool plus100active = false;
	private float plus10sec = 0;
	private float plus100sec = 0;

	private GameObject PandaLevelUpParticle;

	private bool autotap_started = false;
	private float autotap_timer = 0;

	bool tap = false;
	// Use this for initialization
	void Awake(){
		Supersonic.Agent.start();
		string uniqueUserId = SystemInfo.deviceUniqueIdentifier; 
		string appKey = "3b3ec9c9";
		//Init Rewarded Video
		Supersonic.Agent.initRewardedVideo(appKey,uniqueUserId);
		Supersonic.Agent.initOfferwall(appKey,uniqueUserId);
		instance = this;
		level = JuicePrefs.GetInt("GM_level", 1);
		nextd = JuicePrefs.GetDouble("GM_nextd", 0);
		acum = JuicePrefs.GetDouble("GM_nextd", 0);
		damage = JuicePrefs.GetDouble("GM_damage", 1);

		PandaLevelUpParticle = GameObject.FindGameObjectWithTag ("PandaLevelUp");
		PandaLevelUpParticle.SetActive (false);
	}
	void Start () {
		Invoke("StartDPS",0.5f);
		MonsterC = MonstersController.instance;
		if(level<2)
			damagecost = PandaMath.GetUpgradeCost(level, PandaMath.PandaDamageBaseCost);
		else
			damagecost = PandaMath.GetUpgradeCost(level,(PandaMath.PandaDamageBaseCost+(PandaMath.IncreasedEfectiveness(level)*0.75d)));
		nextdamage.text ="+" + NumberToString.instance.Convert(nextd) +" DMG";
		charleveltext.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Game.Levelabb")+": " + level.ToString();
		nextdamageprice.text = NumberToString.instance.Convert(damagecost);


		SupersonicEvents.onOfferwallInitSuccessEvent += OfferwallInitSuccessEvent;
		
		//SupersonicEvents.onOfferwallInitFailEvent += OfferwallInitFailEvent;
		SupersonicEvents.onOfferwallOpenedEvent += OfferwallOpenedEvent;
		
		SupersonicEvents.onOfferwallAdCreditedEvent += OfferwallAdCreditedEvent;
		SupersonicEvents.onOfferwallClosedEvent += OfferwallClosedEvent;
		
		//SupersonicEvents.onOfferwallShowFailEvent += OfferwallShowFailEvent;
		
		//SupersonicEvents.onGetOfferwallCreditsFailEvent += GetOfferwallCreditsFailEvent;


	}
	void StartDPS(){
		StartCoroutine(CalculateDPS());
		StartCoroutine(TucanSpawner());
	}
	IEnumerator TucanSpawner(){
		while(true){
			Instantiate(Tucan);
			yield return new WaitForSeconds(Random.Range(60, 180));
		}
	}
	IEnumerator CalculateDPS(){
		while(true){
			AchievementsController.instance.UpdateDPS(dps*4d + HelpersController.instance.GetTotalDPS());
			if(DPSText!=null)
				DPSText.text = NumberToString.instance.Convert(dps*4f).ToString();
			dps = 0;
			yield return new WaitForSeconds(0.25f);
		}
	}
	// Update is called once per frame
	void FixedUpdate(){
		if(MonsterC.onBoss && (int)MonsterC.mType>1 && secondsLeftToKillBoss>=0){
			secondsLeftToKillBoss-=Time.fixedDeltaTime;
			if(secondsLeftToKillBoss<=0)
				DeactivateBoss();
			monstertimebar.fillAmount = (float)secondsLeftToKillBoss/(float)secondsToKillBoss;
			monstertimetext.text = secondsLeftToKillBoss.ToString("F2");
		}
		if(GoldSystemController.instance.GetGold()<damagecost){
			PandaIncrementer.color = Color.gray;
		}else{
			PandaIncrementer.color = activecolor;
		}

	}

	void Update () {
		int shootindex = 0;
#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0)) {
			isholding = true;
			if (!EventSystem.current.IsPointerOverGameObject()) {
				tap = true;
				AchievementsController.instance.UpdateTaps();
				touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));                
				Object ParticleHit = Instantiate(HitParticle, touchPosition, Quaternion.identity);
				Destroy(ParticleHit, 0.25f);
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			isholding = false;
		}
#else
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			isholding = true;
			touch = Input.GetTouch(0);
			if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId)) {
				tap = true;
				AchievementsController.instance.UpdateTaps();
				touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));                
				Object ParticleHit = Instantiate(HitParticle, touchPosition, Quaternion.identity);
				Destroy(ParticleHit, 0.25f);
			}
		}
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended) {
			isholding = false;
		}
#endif

		if (autotap_started) {
			autotap_timer += Time.deltaTime;
			if(autotap_timer >=30)
			{
				isautotap = false;
				//StopCoroutine("AutoTap");
				autotap_started = false;
				autotap_timer = 0;
			}
		}

		if(tap){
			shootindex = Random.Range(0,3);
			if(shootindex==0)
				PandaAnim.SetTrigger("shoot");
			else if(shootindex==1)
				PandaAnim.SetTrigger("shootdown");
			else
				PandaAnim.SetTrigger("shootup");

			Shooter.SetTrigger("shoot");
			fireshoot.Play();
			Destroy(Instantiate(humito, humitopos.position, Quaternion.identity), 5f);
		}

		if(tap && MonsterC.GetMonsterHP()>0d){
			if(SkillsManager.instance.MoneyShotActive){
				g = System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(false, false));
				g*= SkillsManager.instance.MoneShotValue;
				CoinSystemController.instance.SplitInCoins(g,1);
			}

			MonsterC.Hit();
			currdamage = (damage + PandaMath.GetHelpersDamageBonus() + PandaMath.GetTapDamageBonus()) * PandaMath.DamageAllBonus();
			if(Random.Range(0, 100)>PandaMath.CurrentCritChance()){
				MonsterC.DamageMonster(currdamage);
//				DamageTextPanelController.instance.InstantiateDamageText(NumberToString.instance.Convert(damage));
				dps+=currdamage;
			}else{
				if(MonsterC.onBoss){
					critd = currdamage*Random.Range(PandaMath.TapCritDamageMin, PandaMath.TapCritDamageMax)*PandaMath.CurrentCritDamage()*PandaMath.CurrentCritDamageOnBoss();
				}else{
					critd = currdamage*Random.Range(PandaMath.TapCritDamageMin, PandaMath.TapCritDamageMax)*PandaMath.CurrentCritDamage();
				}
				AchievementsController.instance.UpdateCrits();
				MonsterC.DamageMonster(critd);
//				DamageTextPanelController.instance.InstantiateDamageText((NumberToString.instance.Convert(critd)), true);
				CamShaker.SetTrigger("crit");
				dps+=critd;
			}
		}
		if(MonsterC.GetMonsterHP()<=0d && !MonsterC.isDead){
			//monsterhp = 0;
			AchievementsController.instance.UpdateKills();
			MonsterC.Die();
			MonsterC.isDead = true;
			if(MonsterC.onBoss && (int)MonsterC.mType>1){
				BossButton.SetActive(false);
				BossTimer.SetActive(false);
				g = PandaMath.DecreasedEfectiveness(MonsterC.level)*System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(true, false));
				g = g*PandaMath.GetGoldAllBonus()*PandaMath.GetGoldMonstereBonus();
				if((int)MonsterC.mType>2){
					g = g*PandaMath.GetGoldBossBonus();
					Invoke ("Transition", 0.5f);
					Invoke("ResetMonster", 2.05f);
					if(MonsterC.level>=50 && MonsterC.mType == MonstersController.TypeOfMonster.Boss){
						MagicFlowerSystemController.instance.AddFlowers(1);
					}
				}else{
					Invoke("ResetMonster", 0.33f);
				}
				CoinSystemController.instance.SplitInCoins(g);
			}else{
				g = System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 0.98,0.95)*PandaMath.GetGoldBonus(false, MonsterC.mType==MonstersController.TypeOfMonster.Cage));
				g = g*PandaMath.GetGoldAllBonus();
				if(MonsterC.mType==MonstersController.TypeOfMonster.Cage){
					AchievementsController.instance.UpdateCages();
					g = g*PandaMath.GetGoldCageBonus();
					CoinSystemController.instance.SplitInCoins(g, 10);
				}else{
					CoinSystemController.instance.SplitInCoins(g);
				}
				Invoke("ResetMonster", 0.33f);
			}
		}
		tap = false;
		goldtext.text = NumberToString.instance.Convert(GoldSystemController.instance.GetGold());// gold.ToString();
		goldtext2.text = goldtext3.text = goldtext.text;
		monsterhpbar.fillAmount = (float)(MonsterC.GetMonsterHP()/MonsterC.currentMaxHealth);
		monsterhptext.text = NumberToString.instance.Convert(MonsterC.GetMonsterHP());// monsterhp.ToString();
		tapdamagetext.text = NumberToString.instance.Convert(currdamage) + " DMG";// damage.ToString();
		tapdamagetext2.text = "Tap Damage: " + NumberToString.instance.Convert(damage);
		tapdamagetext3.text = tapdamagetext2.text;
		MonsterLevel.text = MonsterC.level.ToString();
		MonsterCounter.text = MonsterC.monstersKilled.ToString()+"/"+MonsterC.maxMonsters.ToString();
		enemycounter.fillAmount = (float)MonsterC.monstersKilled/(float)MonsterC.maxMonsters;
		if(MonsterC.onBoss){
			BossButton.SetActive(true);
			if((int)MonsterC.mType>1){
				BossButtonText.text = "LEAVE\nBATTLE";
			}else{
				BossButtonText.text = "FIGHT\nBOSS!";
			}
		}else{
			BossButton.SetActive(false);
			BossTimer.SetActive(false);
		}
	}
	public void TapScreen(){
		tap = true;
	}
	void ResetMonster(){

		MonsterC.monstersKilled++;
		if(MonsterC.onBoss && (int)MonsterC.mType>1){
			MonsterC.onBoss = false;
			MonsterC.monstersKilled = 0;
			MonsterC.level++;
			SocialFeatureManager.PostToLeaderBoard(MonsterC.level);
			//GoldSystemController.instance.AddGold(System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(true, false)));
			MonsterC.mType = MonstersController.TypeOfMonster.Normal;
		}
		if(MonsterC.monstersKilled>=MonsterC.maxMonsters){
			MonsterC.monstersKilled = MonsterC.maxMonsters;
			if(!MonsterC.onBoss)
				ActivateBoss();
		}


		//GoldSystemController.instance.AddGold(System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(false, MonsterC.mType==MonstersController.TypeOfMonster.Cage)));
		goldtext.text = NumberToString.instance.Convert(GoldSystemController.instance.GetGold());//gold.ToString();
		MonsterC.totalMonstersKilled++;
		MonsterC.isDead = false;
		MonsterC.SetMonsterHP();
	}
	void ResetMonsterNoBonus(){
		MonsterC.isDead = false;
		MonsterC.SetMonsterHP();
		switch(MonsterC.mType){
		case MonstersController.TypeOfMonster.MiniBoss:{ 	
						Fader.SetTrigger("activate");
						TransText.text=("UnderBoss");
						Transitioner.SetTrigger("trasition");
						break;
					}
		case MonstersController.TypeOfMonster.Boss:{ 	
						Fader.SetTrigger("activate");
						TransText.text=("Big Boss!");
						Transitioner.SetTrigger("trasition");
						break;
					}
		default: 
			break;
		}
	}
	public void IncreaseDamage(){
		if(GoldSystemController.instance.GetGold()>=damagecost*12 && !plus10active){
			plus10sec = 3f;
			StartCoroutine(ActivatePlus10());
		}
		if(GoldSystemController.instance.GetGold()>=damagecost*120 && !plus100active){
			plus100sec = 3f;
			StartCoroutine(ActivatePlus100());
		}
		double prevd;
		if(GoldSystemController.instance.GetGold()>=damagecost){
			if (PandaLevelUpParticle.activeSelf)
				PandaLevelUpParticle.SetActive (false);
			PandaLevelUpParticle.SetActive (true);
			PandaLevelUpParticle.GetComponent<AudioSource>().Play();

			GoldSystemController.instance.SubstractGoldInmediate(damagecost);
			damagecost = PandaMath.GetUpgradeCost(level,(PandaMath.PandaDamageBaseCost+(PandaMath.IncreasedEfectiveness(level)*0.75d)));
			damage += nextd;
			level++;
			charleveltext.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Game.Levelabb")+": " + level.ToString();
			nextdamageprice.text = NumberToString.instance.Convert(damagecost);
			prevd = nextd;
			nextd = (PandaMath.GetUpgradeDamage(level,PandaMath.PandaDamageBase*3/(1+level/1000)))-damage;
		

			if(level>=39 && nextd<11)
				nextd = 11d;
			if(level<39)
				nextd = 10d;
			if(level<37)
				nextd = 9d;
			if(level<35)
				nextd = 8d;
			if(level<32)
				nextd = 7d;
			if(level<28)
				nextd = 6d;
			if(level<25)
				nextd = 5d;
			if(level<21)
				nextd = 4d;
			if(level<17)
				nextd = 3d;
			if(level<12)
				nextd = 2d;
			if(level<7)
				nextd = 1d;
			if(nextd<1d){
				acum = acum+1d;
				nextd = 1d;
			}
			if(level>=39 && prevd<=nextd)
				nextd += System.Math.Floor(prevd*0.14d);
			nextdamage.text ="+" + NumberToString.instance.Convert(nextd) +" DMG";

			JuicePrefs.SetInt("GM_level", level);
			JuicePrefs.SetDouble("GM_nextd", nextd);
			JuicePrefs.SetDouble("GM_damage", damage);
			JuicePrefs.SetTimeStamp();
		}
	}

	IEnumerator ActivatePlus10(){
		plus10active = true;
		plus10.SetActive(true);
		while(plus10sec>0){
			plus10sec -= 0.5f;
			yield return new WaitForSeconds(0.5f);
		}
		plus10active = false;
		plus10.SetActive(false);
	}
	IEnumerator ActivatePlus100(){
		plus100active = true;
		plus100.SetActive(true);
		while(plus100sec>0){
			plus100sec -= 0.5f;
			yield return new WaitForSeconds(0.5f);
		}
		plus100active = false;
		plus100.SetActive(false);
	}
	
	public void Increase10(){
		for(int i=0; i<10; i++){
			IncreaseDamage();
		}
	}
	public void Increase100(){
		for(int i=0; i<100; i++){
			IncreaseDamage();
		}
	}


	public void ActivateBoss(){
		if((int)MonsterC.mType>1){
			DeactivateBoss();
			return;
		}
		BossTimer.SetActive(true);
		secondsLeftToKillBoss = secondsToKillBoss;
		MonsterC.onBoss = true;
		MonsterC.retrieve_boss = true;
		if(MonsterC.level%5==0)
			MonsterC.mType = MonstersController.TypeOfMonster.Boss;
		else
			MonsterC.mType = MonstersController.TypeOfMonster.MiniBoss;
		ResetMonsterNoBonus();
	}
	public void DeactivateBoss(){
		MonsterC.oldBoss = MonsterC.currentMonster;
		BossTimer.SetActive(false);
		MonsterC.mType = MonstersController.TypeOfMonster.Normal;
		ResetMonsterNoBonus();
	}
	void Transition(){
		Fader.SetTrigger("fade");
		TransText.text=("New Enviroment");
		Transitioner.SetTrigger("trasition");
		BackgroundChanger.instance.ChangeNext();
	}
	IEnumerator AutoTap(int tps){
		float timeinit = Time.unscaledTime;
		Debug.Log ("unscaledTime=" + timeinit);
		float taptime;
		while(Time.unscaledTime-timeinit<30 || true){
			taptime = Time.unscaledTime;
			while(Time.unscaledTime-taptime<1f/(float)tps){
				yield return new WaitForEndOfFrame();
			}
			if(isautotap && isholding)
				tap = true;
		}
		isautotap = false;
		isholding = false;
	}
	public void StartAutoTap(int tps){
		if (!autotap_started && MagicBeanSystemController.instance.GetTotalBeans()>=100) {
			autotap_started = true;
			MagicBeanSystemController.instance.AddBeans(-100);
			StartCoroutine (AutoTap (tps));
			isautotap = true;
			NotificationController.instance.SetNotification ("Hold for Auto Tap 30taps/sec.", Pawtomatic, 30);
			NotificationController.instance.Show ();
		}
	}
	public void GoldMountain(){
		if(MagicBeanSystemController.instance.GetTotalBeans()>=100){
			MagicBeanSystemController.instance.AddBeans(-100);
			g = System.Math.Floor(PandaMath.GetFibonacciForLevel(MonsterC.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(false, false));
			if(g<1) g=1;
			g*=100;
			CoinSystemController.instance.SplitInCoins(g, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y+1, 0), 15);
		}
	}
	void OnApplicationPause(bool isPaused) { 
		
		if (isPaused) {
			
			Supersonic.Agent.onPause ();
			
		}else{                   
			
			Supersonic.Agent.onResume ();
			
		}
		
	}
	public void showad(){
		Supersonic.Agent.showRewardedVideo();
	}
	public void showwall(){
		if(Supersonic.Agent.isOfferwallAvailable()){
			Supersonic.Agent.showOfferwall();
		}
	}
	/**
 * Invoked when the Offerwall is prepared an ready to be shown to the user
 */ 
	
	void OfferwallInitSuccessEvent(){
		Debug.Log("offer wall init");
	}
	
	/**
 * Invoked when the Offerwall does not load for the user
 */ 
	
	void OfferwallInitFailEvent(string desc){
		Debug.Log("offer wall fail init");
	}
	
	/**
 * Invoked when the Offerwall successfully loads for the user.
 */ 
	
	void OfferwallOpenedEvent(){
		Debug.Log("offer wall open");
	}
	
	/**
 * Invoked when the method 'showOfferWall' is called and the OfferWall fails to load.  //@param desc - A string which represents the reason of 'showOfferwall' failure.

 */
	
	void OfferwallShowFailEvent(string desc){
		Debug.Log("offer wall open fail");
	}
	
	/**
  * Invoked each time the user completes an Offer.
  * Award the user with the credit amount corresponding to the value of the ‘credits’ 
  * parameter.
  * @param dict - A dictionary which holds the credits and the total credits.   
  */
	void OfferwallAdCreditedEvent(Dictionary<string,object> dict){
		
		Debug.Log ("I got OfferwallAdCreditedEvent, current credits = " + dict["credits"] + "totalCredits = " + dict["totalCredits"]);
		int b = int.Parse(dict["credits"].ToString());
		MagicBeanSystemController.instance.AddBeans(b);
	}
	
	/**
  * Invoked when the method 'getOfferWallCredits' fails to retrieve 
  * the user's credit balance info.
  * @param desc - A string object which represents the reason of 'getOffereallCredits' failure. 
  */
	void GetOfferwallCreditsFailEvent(string desc){
		Debug.Log("offer wall fail credits");
	}
	
	/**
  * Invoked when the user is about to return to the application after closing 
  * the Offerwall.
  */
	void OfferwallClosedEvent(){
		Debug.Log("offer wall closed");
		Supersonic.Agent.getOfferwallCredits();
		
	}
	public void ShareOnFB(){
		if(JuicePrefs.GetInt("FB_Ssharedone",0)==1){
			NotificationController.instance.SetNotification("Facebook Share already claimed, try again tomorrow", null, 3);
			return;
		}
		if(FacebookManager.instance.isInit){
			if(FB.IsLoggedIn){
				FacebookManager.instance.FBShare(ShareCallback);
			}else{
				FacebookManager.instance.Login();
			}
		}else{
			FacebookManager.instance.CallFBInit();
		}
	}
	private void ShareCallback(FBResult result){
		
		
		if(result.Error==null){
			if (result.Text == "{\"cancelled\":true}")
			{
				Debug.LogWarning("FB Sharing was cancelled.");
				return;
			}else{
				//SocialFeatureManager.UnlockAchievement(LinesAchievement.achievementTwoLinesLover);
				MagicBeanSystemController.instance.AddBeans(100);
				JuicePrefs.SetInt("FB_Ssharedone",1);
			//	GA.API.Design.NewEvent("GAME:SHARE:FACEBOOKSHAREAPP", 1);
			}
		}
	}
	public void openLeaderBoard(){
		SocialFeatureManager.ShowLeaderBoard();
	}
	public void openAchievements(){
		SocialFeatureManager.ShowAchievementsUI();
	}


}
