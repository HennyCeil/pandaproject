﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class InfoBarController : MonoBehaviour {
	public enum InfoType{
		RESTART,
		GADGETS
	}
	private string[] InfoKey = {"Character.Restart", "Helpers.Gadgets"};
	public InfoType InfoID;
	public Text Title;
	public Text Description;
	public Image Background;
	public Image IncrementalButton;
	public Text IncrementalText;
	public Color BackgroundColor;

	private LanguageManager LangManager;
	public bool UseIncrementalButton = true;
	// Use this for initialization
	void Start () {
		LangManager = LanguageManager.Instance;
		IncrementalButton.gameObject.SetActive(UseIncrementalButton);
		Title.text = LangManager.GetTextValue(InfoKey[(int)InfoID] + ".Title");
		Description.text = LangManager.GetTextValue(InfoKey[(int)InfoID] + ".Description");
		if(InfoID != InfoType.GADGETS)
			IncrementalText.text = LangManager.GetTextValue(InfoKey[(int)InfoID] + ".Button");
		Background.color = BackgroundColor;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
