﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class FruitClass : MonoBehaviour {
	public enum FruitType{
		COCONUT,
		BANANA,
		STRAWBERRY,
		MANGO,
		APPLE,
		BLUEBERRY,
		PEAR,
		CHERRY,
		ORANGE
	}
	private string[] FruitKey = {"Coconut", "Banana", "Strawberry", "Mango", "Apple", "Blueberry", "Pear", "Cherry", "Orange"};
	//UIElements
	public FruitType FruitID;
	public Text TitleText;
	public Text LevelText;
	public Image Icon;
	public Image IncrementalButton;
	public Text IncrementalText;
	public Text nextPrice;
	public Text nextValueText;
	public Text ValueText;

	//HelperStats
	private int currentLevel = 0;
	public double currentValue;
	private double baseValue;
	private int basecost;
	private int currentcost;
	private double nextValue;
	public string IdFruit;

	private LanguageManager LM;
	private string FruitName;
	private string FruitDescription;
	private bool Unlocked = false;

	void Awake(){
		IdFruit = "Fruits." + FruitKey[(int)FruitID];
		Unlocked = JuicePrefs.GetInt(IdFruit+"Unlocked", 0)==1;
	}
	
	// Use this for initialization
	void Start () {
		Invoke("InitFruit", 0.1f);
		LM = LanguageManager.Instance;
		LM.OnChangeLanguage += LanguageSetup;
		LanguageSetup(LM);
	}
	public void InitFruit(){
		//load()
		basecost = PandaMath.FruitsBaseCost[(int)FruitID];
		baseValue = PandaMath.FruitsBaseValue[(int)FruitID];
		currentcost = JuicePrefs.GetInt(IdFruit+"currentcost", basecost);
		currentValue = JuicePrefs.GetDouble(IdFruit+"currentDPS",0);
		currentLevel = JuicePrefs.GetInt(IdFruit+"currentLevel",0);
		nextValue = JuicePrefs.GetDouble(IdFruit+"nextValue", baseValue);
		if(nextValue<1)
			nextValue = baseValue;
		nextPrice.text = NumberToString.instance.Convert(currentcost);
		nextValueText.text = "+"+NumberToString.instance.Convert(baseValue);
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		ValueText.text = NumberToString.instance.Convert(currentValue)+ "  "  +  LM.GetTextValue("Game.DPSabb");
	}
	// Update is called once per frame
	void FixedUpdate () {
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		ValueText.text = NumberToString.instance.Convert(currentValue)+ "  "  +  LM.GetTextValue("Game.DPSabb");
		if(MagicFlowerSystemController.instance.GetTotalFlowers()<currentcost){
			IncrementalButton.color = Color.gray;
		}else{
			IncrementalButton.color = GameManager.instance.activecolor;
		}
		if(!Unlocked)
			IncrementalText.text = LM.GetTextValue("Game.Unlock");
		else
			IncrementalText.text = LM.GetTextValue("Game.LevelUp");
	}
	public void IncreaseFruitLevel(){
		if(PandaMath.FruitsLevelCap[(int)FruitID]==0 || currentLevel < PandaMath.FruitsLevelCap[(int)FruitID]){
			if(MagicFlowerSystemController.instance.GetTotalFlowers()>=currentcost){
				if(!Unlocked){
					Unlocked = true;
					JuicePrefs.SetInt(IdFruit+"Unlocked", 1);
				}
				MagicFlowerSystemController.instance.AddFlowers(-currentcost);
				currentValue += nextValue;
				currentLevel++;
				currentcost = (int)(PandaMath.GetUpgradeCost(currentLevel*PandaMath.FruitsIncrementMultiplierCost[(int)FruitID],PandaMath.FruitsIncrementBaseCost[(int)FruitID]));
				nextValue += PandaMath.FruitsIncrementValue[(int)FruitID];

				LevelText.text = "Lvl: " + currentLevel.ToString();
				nextPrice.text = NumberToString.instance.Convert(currentcost);
				nextValueText.text ="+" + NumberToString.instance.Convert(nextValue) + LM.GetTextValue("Game.DMGabb");
				JuicePrefs.SetInt(IdFruit+"currentcost", currentcost);
				JuicePrefs.SetDouble(IdFruit+"currentValue",currentValue);
				JuicePrefs.SetInt(IdFruit+"currentLevel",currentLevel);
				JuicePrefs.SetDouble(IdFruit+"nextValue",nextValue);

			}
		}
	}
	public void OpenInfo(){
		/*HelpersInfoUI.instance.SetSkills(Skills, SkillIsActive);
		HelpersInfoUI.instance.SetIcon(Icon.sprite, reset);
		HelpersInfoUI.instance.SetWindow(IdFruit, currentLevel, currentValue);
		HelpersInfoUI.instance.Show();*/
	}
	public void LanguageSetup(LanguageManager langm){
		FruitName = langm.GetTextValue(IdFruit + ".Name");
		FruitDescription = langm.GetTextValue(IdFruit + ".Name");
		TitleText.text = FruitName;
	}
}
