﻿using UnityEngine;
using System.Collections;

public class PlayAudioShoot : MonoBehaviour {
	public AudioClip[] Clips;
	public bool PlayRandom = true;
	public Vector2 RandomPitch = new Vector2(1,1);
	AudioSource audio;
	int c = 0;
	void Start(){
		audio = this.GetComponent<AudioSource>();
	}
	// Use this for initialization
	public void PlayShoot(){
		if(PlayRandom){
			c = Random.Range(0, 1);
		}else{
			c++;
			if(c >= Clips.Length)
				c=0;
		}
		audio.pitch = Random.Range(RandomPitch.x, RandomPitch.y);
		audio.PlayOneShot(Clips[c]);
	}

	public void PlayFuryShoot(){
		audio.pitch = 1;
		audio.PlayOneShot(Clips[2]);
	}
}
