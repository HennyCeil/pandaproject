using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class CharacterBarController : MonoBehaviour {
	public Text Title;
	public Text Level;
	public Text Damage;
	public Text Price;
	public Text LvlUp;
	public Text PlusDamage;
	public Image IncrementButton;
	public Color32 buttoncolor;
	private LanguageManager LangManager;
	// Use this for initialization
	void Start () {
		Title.text = "Panda Defender";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
