﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollPanelController : MonoBehaviour {
	bool showing = false;
	public Image buttonActivated;
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = this.GetComponent<Animator>();
		//hide();
	}

	public void hide(){
		anim.SetBool("show", false);
		showing = false;
		buttonActivated.color = Color.clear;
	}

	public void show(){
		anim.SetBool("show", true);
		showing = true;
		buttonActivated.color = Color.white;
	}

	public void toggle(){
		if(showing){
			hide();
		}else{
			show();
		}
	}
}
