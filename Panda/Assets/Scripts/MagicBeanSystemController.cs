﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class MagicBeanSystemController : MonoBehaviour {
	public static MagicBeanSystemController instance;
	private readonly object syncLock = new object();
	private Queue<int> MagicBeanQueue;
	private int magicbeans;
	private int b;
	public Text counter;
	public Text totalbeans;
	public Animator BeanCounterAnimator;
	private bool showingcounter = false;
	private float secstohide;
	// Use this for initialization
	void Awake(){
		instance = this;
		magicbeans = JuicePrefs.GetInt("Beans_amount",0);
	}
	void Start () {
		MagicBeanQueue = new Queue<int>();

	}

	// Update is called once per frame
	void Update () {
		while(MagicBeanQueue.Count>0){
			lock(syncLock){
				b = MagicBeanQueue.Dequeue();
				magicbeans+=b;
				b=0;
				JuicePrefs.SetInt("Beans_amount",magicbeans);
				if(!showingcounter){
					BeanCounterAnimator.SetTrigger("show");
				}
				showingcounter = true;
				secstohide = 3f;
			}
			counter.text = magicbeans.ToString();
			totalbeans.text = magicbeans.ToString();
		}
		if(showingcounter){
			secstohide-=Time.deltaTime;
			if(secstohide <=0)
				HideBeanCounter();
		}


	}
	void HideBeanCounter(){
		BeanCounterAnimator.SetTrigger("hide");
		showingcounter = false;
	}
	public double GetTotalBeans(){
		return magicbeans;
	}
	public void AddBeans(int v){
		lock(syncLock){
			MagicBeanQueue.Enqueue(v);
		}
	}
}
