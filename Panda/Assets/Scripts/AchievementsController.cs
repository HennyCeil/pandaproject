﻿using UnityEngine;
using System.Collections;

public class AchievementsController : MonoBehaviour {
	public static AchievementsController instance;
	double totalgold;
	int totalcrits;
	double totaldps;
	int totalkills;
	int totaltaps;
	int totallevels;
	int totalopenseeds;
	int totalfury;
	int totalfruits;
	int totalorquids;
	int totalresets;
	int totalcages;
	int totalbirds;
	int stepgold;
	int stepcrits;
	int stepdps;
	int stepkills;
	int steptaps;
	int steplevels;
	int stepopenseeds;
	int stepfury;
	int stepfruits;
	int steporquids;
	int stepresets;
	int stepcages;
	int stepbirds;

	public AchievementObject achobjgold;
	public AchievementObject achobjcrits;
	public AchievementObject achobjdps;
	public AchievementObject achobjkills;
	public AchievementObject achobjtaps;
	public AchievementObject achobjlevels;
	public AchievementObject achobjopenseeds;
	public AchievementObject achobjfury;
	public AchievementObject achobjfruits;
	public AchievementObject achobjorquids;
	public AchievementObject achobjresets;
	public AchievementObject achobjcages;
	public AchievementObject achobjbirds;

	public GameObject achievecontainer;
	// Use this for initialization
	void Awake () {
		instance = this;
	}
	void FixedUpdate(){
		if(achievecontainer.activeInHierarchy){
			achobjgold.UpdateAchievement(totalgold);
			achobjcrits.UpdateAchievement(totalcrits);
			achobjdps.UpdateAchievement(totaldps);
			achobjkills.UpdateAchievement(totalkills);
			achobjtaps.UpdateAchievement(totaltaps);
			achobjlevels.UpdateAchievement(totallevels);
			achobjopenseeds.UpdateAchievement(totalopenseeds);
			achobjfury.UpdateAchievement(totalfury);
			achobjfruits.UpdateAchievement(totalfruits);
			achobjorquids.UpdateAchievement(totalorquids);
			achobjresets.UpdateAchievement(totalresets);
			achobjcages.UpdateAchievement(totalcages);
			achobjbirds.UpdateAchievement(totalbirds);
		}
	}
	void Start(){
		totalgold = JuicePrefs.GetDouble("Ach_totalgold", 0);
		totaldps = JuicePrefs.GetDouble("Ach_totaldps", 0);
		totalcrits = JuicePrefs.GetInt("Ach_totalcrits", 0);
		totaltaps = JuicePrefs.GetInt("Ach_totaltaps", 0);
		totalkills = JuicePrefs.GetInt("Ach_totalkills", 0);
		totallevels = JuicePrefs.GetInt("Ach_totallevels", 0);
		totalopenseeds = JuicePrefs.GetInt("Ach_totalopenseeds", 0);
		totalfury = JuicePrefs.GetInt("Ach_totalfury", 0);
		totalfruits = JuicePrefs.GetInt("Ach_totalfruits", 0);
		totalorquids = JuicePrefs.GetInt("Ach_totalorquids", 0);
		totalcages = JuicePrefs.GetInt("Ach_totalcages", 0);
		totalbirds = JuicePrefs.GetInt("Ach_totalbirds", 0);
		totalresets = JuicePrefs.GetInt("Ach_totalresets", 0);

		stepgold = JuicePrefs.GetInt("Ach_stepgold", 0);
		stepcrits = JuicePrefs.GetInt("Ach_stepcrits", 0);
		stepdps = JuicePrefs.GetInt("Ach_stepdps", 0);
		stepkills = JuicePrefs.GetInt("Ach_stepkills", 0);
		steptaps = JuicePrefs.GetInt("Ach_steptaps", 0);
		steplevels = JuicePrefs.GetInt("Ach_steplevels", 0);
		stepopenseeds = JuicePrefs.GetInt("Ach_stepopenseeds", 0);
		stepfury = JuicePrefs.GetInt("Ach_stepfury", 0);
		stepfruits = JuicePrefs.GetInt("Ach_stepfruits", 0);
		steporquids = JuicePrefs.GetInt("Ach_steporquids", 0);
		stepresets = JuicePrefs.GetInt("Ach_stepresets", 0);
		stepcages = JuicePrefs.GetInt("Ach_stepcages", 0);
		stepbirds = JuicePrefs.GetInt("Ach_stepbirds", 0);

	}
	// Update is called once per frame
	public void UpdateDPS (double dps) {
		if(dps > totaldps){
			totaldps=dps;
			JuicePrefs.SetDouble("Ach_totaldps", totaldps);
		}
	}
	public void UpdateGold (double g) {
		totalgold+=g;
		JuicePrefs.SetDouble("Ach_totalgold", totalgold);
	}
	public void UpdateTaps () {
		totaltaps++;
		JuicePrefs.SetInt("Ach_totaltaps", totaltaps);
	}
	public void UpdateCrits () {
		totalcrits++;
		JuicePrefs.SetInt("Ach_totalcrits", totalcrits);
	}
	public void UpdateKills () {
		totalkills++;
		JuicePrefs.SetInt("Ach_totalkills", totalkills);
	}
	public void UpdateLevels () {
		totallevels++;
		JuicePrefs.SetInt("Ach_totallevels", totallevels);
	}
	public void UpdateFruits () {
		totalfruits++;
		JuicePrefs.SetInt("Ach_totalfruits", totalfruits);
	}
	public void UpdateFury () {
		totalfury++;
		JuicePrefs.SetInt("Ach_totalfury", totalfury);
	}
	public void UpdateOrquids () {
		totalorquids++;
		JuicePrefs.SetInt("Ach_totalorquids", totalorquids);
	}
	public void UpdateOpenseed () {
		totalopenseeds++;
		JuicePrefs.SetInt("Ach_totalopenseeds", totalopenseeds);
	}
	public void UpdateBirds () {
		totalbirds++;
		JuicePrefs.SetInt("Ach_totalbirds", totalbirds);
	}
	public void UpdateCages () {
		totalcages++;
		JuicePrefs.SetInt("Ach_totalcages", totalcages);
	}
	public void UpdateResets () {
		totalresets++;
		JuicePrefs.SetInt("Ach_totalresets", totalresets);
	}
}
