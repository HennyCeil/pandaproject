﻿using UnityEngine;
using System.Collections;

public class OrquidSystemController : MonoBehaviour {

	public static OrquidSystemController instance;
	public MagicFlowerController[] Flowers;
	private int current = 0;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		Flowers = new MagicFlowerController[transform.childCount];
		for(int i = 0; i < Flowers.Length; i++){
			Flowers[i] = transform.GetChild(i).GetComponent<MagicFlowerController>();
		}
	}
	
	public void InstantiateFlower(int value, Vector3 pos){
		Flowers[current].value = value;
		Flowers[current].pos = pos;
		Flowers[current].gameObject.SetActive(true);
		current++;
		if(current == Flowers.Length)
			current = 0;
	}
	public void InstantiateFlower(int value){
		Flowers[current].value = value;
		Flowers[current].gameObject.SetActive(true);
		current++;
		if(current == Flowers.Length)
			current = 0;
	}
}
