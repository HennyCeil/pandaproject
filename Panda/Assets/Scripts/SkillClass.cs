﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;


public class SkillClass : MonoBehaviour {
	public enum SkillType{
		FURY=0,
		CLONE,
		CRITICAL,
		HELPERSRAGE,
		NATUREWRATH,
		MONEYSHOT
	}
	private string[] SkillKey = {"Fury", "Clone", "Critical", "Helpersrage", "Naturewrath", "Moneyshot"};
	private int[] SkillLevelUnlock = 	{50,100,200,300,400,500};
	private int[] SkillBaseValue = 		{10,10,	30,	50,	50,	10};
	private int[] SkillIncrementValue = {10,3,	1,	5,	10,	2};
	//UIElements
	public SkillType SkillID;
	public Text TitleText;
	public Text LevelText;
	public Image Icon;
	public Image IncrementalButton;
	public Text IncrementalText;
	public Text nextPrice;
	public Text nextValue;
	public Text ValueText;
	public Image SkillFiller;
	public Text CoolDownText;
	public Image SkillBar;
	public Animator PandaAnimator;
	//Helper Object, animator
	public GameObject SkillObject;

	//HelperStats
	private int currentLevel = 0;
	private double value;
	private double currentvalue;
	private double basevalue;
	private double basecost;
	private double currentcost;
	public string IdSkill;

	private LanguageManager LM;
	private string SkillName;
	private string SkillDescription;
	private bool Unlocked = false;
	public bool Available = false;
	public float CoolDown = 60;
	public float duration = 30;
	private float timeused = -50000;
	private float timeinit = -50000;
	private float timerestore = -50000;
	private bool OnCD = false;
	private bool beingUsed = false;
	
	private MonstersController MonsterC;
	
	
	// Use this for initialization
	void Start () {
		IdSkill = "Skill." + SkillKey[(int)SkillID];
		Invoke("InitSkill", 0.5f);
		LM = LanguageManager.Instance;
		LM.OnChangeLanguage += LanguageSetup;
		LanguageSetup(LM);
		MonsterC = MonstersController.instance;
	}

	public void InitSkill(){
		Unlocked = JuicePrefs.GetInt(IdSkill+"Unlocked", 0)==1;
		currentLevel = JuicePrefs.GetInt(IdSkill+"currentLevel", 0);
		if(!Unlocked)
			SkillObject.SetActive(false);
		else
			SkillObject.SetActive(true);
		basecost = 2*PandaMath.GetUpgradeCost(SkillLevelUnlock[(int)SkillID],5);
		basevalue = SkillBaseValue[(int)SkillID];
		currentcost = JuicePrefs.GetDouble(IdSkill+"currentcost", basecost);
		currentvalue = JuicePrefs.GetDouble(IdSkill+"currentvalue",basevalue);
		nextPrice.text = NumberToString.instance.Convert(currentcost);
		nextValue.text ="+" + SkillIncrementValue[(int)SkillID].ToString();
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		ValueText.text = NumberToString.instance.Convert(currentvalue)+ "  "  +  LM.GetTextValue(IdSkill+".Value");
	}
	// Update is called once per frame
	void FixedUpdate () {
		if(LM==null)
			LM = LanguageManager.Instance;
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		ValueText.text = NumberToString.instance.Convert(currentvalue)+ "  "  +  LM.GetTextValue(IdSkill+".Value");
		if(GoldSystemController.instance.GetGold()<currentcost || GameManager.instance.level<SkillLevelUnlock[(int)SkillID]){
			IncrementalButton.color = Color.gray;
		}else{
			IncrementalButton.color = GameManager.instance.activecolor;
		}
		if(!Unlocked)
			IncrementalText.text = LM.GetTextValue("Game.Unlock") + " at lvl " + SkillLevelUnlock[(int)SkillID];
		else
			IncrementalText.text = LM.GetTextValue("Game.LevelUp");
		
		if(Time.unscaledTime - timeused < CoolDown){
			Available = false;
			CoolDownText.enabled = true;
			CoolDownText.text = PandaMath.SecsToString(CoolDown - (Time.unscaledTime - timeused));
			SkillFiller.fillAmount = (Time.unscaledTime - timeused)/CoolDown;
		}else{
			if(!beingUsed){
				Available = true;
				CoolDownText.enabled = false;
				SkillFiller.fillAmount = 1;
			}else{
				CoolDownText.enabled = true;
				CoolDownText.text = PandaMath.SecsToString(duration - (Time.unscaledTime - timeinit));
			}
		}
	}
	public void IncreaseSkillLevel(){
		if(GoldSystemController.instance.GetGold()>=currentcost && GameManager.instance.level>=SkillLevelUnlock[(int)SkillID]){
			if(!Unlocked){
				Unlocked = true;
				JuicePrefs.SetInt(IdSkill+"Unlocked", 1);
			}
			SkillObject.SetActive(true);
			GoldSystemController.instance.AddGold(-currentcost);
			currentvalue += SkillIncrementValue[(int)SkillID];
			currentLevel++;
			currentcost = PandaMath.GetUpgradeCost(currentLevel*25,basecost);
			LevelText.text = "Lvl: " + currentLevel.ToString();
			nextPrice.text = NumberToString.instance.Convert(currentcost);
			JuicePrefs.SetInt(IdSkill+"currentLevel", currentLevel);
		}
	}
	/*public void OpenInfo(){
		HelpersInfoUI.instance.SetSkills(Skills, SkillIsActive);
		HelpersInfoUI.instance.SetIcon(Icon.sprite, reset);
		HelpersInfoUI.instance.SetWindow(IdHelper, currentLevel, currentDPS);
	}*/
	public void LanguageSetup(LanguageManager langm){
		SkillName = langm.GetTextValue(IdSkill + ".Name");
		SkillDescription = langm.GetTextValue(IdSkill + ".Description");
		//skillv = langm.GetTextValue(IdSkill + ".Description");
		TitleText.text = SkillName;
	}
	/*public void UnlockHelper(){
		Unlocked = true;
	}
	public void ResetHelper(){
		reset = true;
	}*/
	IEnumerator Fury(){
		SkillBar.color = Color.green;
		PandaAnimator.SetTrigger("fury");
		yield return new WaitForSeconds(2f);
		double dmg = GameManager.instance.damage*currentvalue;
		bool crit = false;
		if(Random.Range(0f, 1f) <= PandaMath.CurrentCritChance()/100f){
			crit = true;
			dmg*=Random.Range(PandaMath.TapCritDamageMin, PandaMath.TapCritDamageMax)*PandaMath.CurrentCritDamage();
		}
		MonstersController.instance.DamageMonster(dmg, false);
		DamageTextPanelController.instance.InstanciateFuryDamage(NumberToString.instance.Convert(dmg), crit);
		GameManager.instance.CamShaker.SetTrigger("crit");
		OnCD = true;
		timeused = Time.unscaledTime;
		beingUsed = false;
		SkillBar.color = Color.white;
	}
	IEnumerator Clone(){
		float t = Time.time;
		double dmg = GameManager.instance.damage;
		SkillBar.color = Color.green;
		while(Time.time-t <= duration){
			//SkillBar.color = Color.Lerp(Color.grey, Color.white, (Time.time-t)/duration);
			yield return new WaitForSeconds(1.0f/(float)currentvalue);
			MonstersController.instance.DamageMonster(dmg);
		}
		timeused = Time.unscaledTime;
		OnCD = true;
		beingUsed = false;
		
		SkillBar.color = Color.white;
	}
	IEnumerator HelperSpeed(){
		SkillBar.color = Color.green;
		SkillsManager.instance.helpersSpeed = 1.0f+(float)currentvalue/100.0f;
		yield return new WaitForSeconds(duration);
		SkillsManager.instance.helpersSpeed = 1;
		timeused = Time.unscaledTime;
		OnCD = true;
		beingUsed = false;
		SkillBar.color = Color.white;
	}
	IEnumerator Critical(){
		SkillBar.color = Color.green;
		SkillsManager.instance.critchanceskill = (float)currentvalue;
		yield return new WaitForSeconds(duration);
		SkillsManager.instance.critchanceskill = 0;
		timeused = Time.unscaledTime;
		OnCD = true;
		beingUsed = false;
		SkillBar.color = Color.white;
	}
	IEnumerator Wrath(){
		SkillBar.color = Color.green;
		SkillsManager.instance.wrathdamaebonus = 1.0f + (float)currentvalue/100.0f;
		yield return new WaitForSeconds(duration);
		SkillsManager.instance.wrathdamaebonus = 1;
		timeused = Time.unscaledTime;
		OnCD = true;
		beingUsed = false;
		SkillBar.color = Color.white;
	}
	IEnumerator Money(){
		SkillBar.color = Color.green;
		SkillsManager.instance.MoneShotValue = (float)currentvalue/100.0f;
		SkillsManager.instance.MoneyShotActive = true;
		yield return new WaitForSeconds(duration);
		SkillsManager.instance.MoneShotValue = 0;
		SkillsManager.instance.MoneyShotActive = false;
		timeused = Time.unscaledTime;
		OnCD = true;
		beingUsed = false;
		SkillBar.color = Color.white;
	}
	public void ActivateSkill(){
		if(Unlocked && Available && !beingUsed){
			beingUsed = true;
			timeinit = Time.unscaledTime;
			//HelperAnim.SetTrigger("attack");
			switch(SkillID){
			case SkillType.FURY: StartCoroutine(Fury()); return;
			case SkillType.CLONE: StartCoroutine(Clone()); return;
			case SkillType.CRITICAL: StartCoroutine(Critical()); return;
			case SkillType.HELPERSRAGE: StartCoroutine(HelperSpeed()); return;
			case SkillType.MONEYSHOT: StartCoroutine(Money()); return;
			case SkillType.NATUREWRATH: StartCoroutine(Wrath()); return;
			}
		}
	}

	public void ActivateSkillGift(){
		if(!beingUsed){
			NotificationController.instance.SetNotification(SkillDescription, Icon.sprite, duration);
			NotificationController.instance.Show();
			beingUsed = true;
			timeinit = Time.unscaledTime;
			timerestore = timeused;
			Invoke("ResetCooldown", duration+2);
			//HelperAnim.SetTrigger("attack");
			switch(SkillID){
			case SkillType.FURY: StartCoroutine(Fury()); return;
			case SkillType.CLONE: StartCoroutine(Clone()); return;
			case SkillType.CRITICAL: StartCoroutine(Critical()); return;
			case SkillType.HELPERSRAGE: StartCoroutine(HelperSpeed()); return;
			case SkillType.MONEYSHOT: StartCoroutine(Money()); return;
			case SkillType.NATUREWRATH: StartCoroutine(Wrath()); return;
			}
		}
	}
	public void ResetCooldown(){
		timeused = timerestore;
	}

}
