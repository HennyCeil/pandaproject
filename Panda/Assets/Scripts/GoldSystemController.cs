﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GoldSystemController : MonoBehaviour {
	public static GoldSystemController instance;
	public GameObject idlegoldbutton;
	private readonly object syncLock = new object();
	private Queue<double> GoldQueue;
	private double gold;
	private double g;
	private double IdleMoney;
	private DateTime datenow;
	private DateTime datelast;
	// Use this for initialization
	void Awake(){
		instance = this;
		gold = JuicePrefs.GetDouble("Gold_amount",0);
	}
	void Start () {
		GoldQueue = new Queue<double>();

	}
	void OnEnable(){
		Invoke("checkidlemoney", 3);
	}
	// Update is called once per frame
	void Update () {
		while(GoldQueue.Count>0){
			lock(syncLock){
				g = GoldQueue.Dequeue();
				gold+=g;
				if(g>0)
					AchievementsController.instance.UpdateGold(g);
				g=0;
				JuicePrefs.SetDouble("Gold_amount",gold);

			}
		}
	}
	void checkidlemoney(){
		CancelInvoke("checkidlemoney");
		datenow = UnbiasedTime.Instance.Now();
		datelast = this.ReadTimestamp("IdleMoney_LastDate", datenow);
		TimeSpan elapsed = datenow - datelast;
		float totalElapsedSeconds = elapsed.Days*24*60*60 + elapsed.Hours*60*60 + elapsed.Minutes*60 + elapsed.Seconds;
		Debug.Log("Total elapsed seconds since last sesion: " + totalElapsedSeconds.ToString());
		if(totalElapsedSeconds>=1){
			double g = System.Math.Floor(PandaMath.GetFibonacciForLevel(MonstersController.instance.level, 1, 1, 0.98,0.95)*PandaMath.GetGoldBonus(false, false));
			IdleMoney = System.Math.Floor(totalElapsedSeconds*(HelpersController.instance.GetTotalDPS()/MonstersController.instance.maxHealth))*g;
			this.WriteTimestamp("IdleMoney_LastDate", UnbiasedTime.Instance.Now());
			Debug.Log("Total Idle Money: " + NumberToString.instance.Convert(IdleMoney));
			if(IdleMoney>0)
				idlegoldbutton.SetActive(true);
		}
	}
	public double GetGold(){
		return gold;
	}
	public void AddGold(double v){
		lock(syncLock){
			GoldQueue.Enqueue(v);
		}
	}
	void OnApplicationPause(bool pauseStatus) {
		if(!pauseStatus)
			Invoke("checkidlemoney", 3);
		else
			this.WriteTimestamp("IdleMoney_LastDate", UnbiasedTime.Instance.Now());
	}
	void OnApplicationQuit() {
		this.WriteTimestamp("IdleMoney_LastDate", UnbiasedTime.Instance.Now());
		JuicePrefs.SetTimeStamp();
	}
	public void AddIdleGold(){
		idlegoldbutton.SetActive(false);
		Vector3 pos = Camera.main.transform.position;
		pos.z = 0;
		pos.y +=6.5f;
		CoinSystemController.instance.SplitInCoins(System.Math.Floor(IdleMoney),pos);
		IdleMoney = 0;
	}
	private DateTime ReadTimestamp (string key, DateTime defaultValue) {
		long tmp = Convert.ToInt64(JuicePrefs.GetString(key, "0"));
		if ( tmp == 0 ) {
			return defaultValue;
		}
		return DateTime.FromBinary(tmp);
	}
	
	private void WriteTimestamp (string key, DateTime time) {
		JuicePrefs.SetString(key, time.ToBinary().ToString());
	}
	public void SubstractGoldInmediate(double v){
		gold -= v;
	}

}
