﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DamageTextController : MonoBehaviour {
	public Animator TextAnim;
	public Text DamageText;
	public int initialfontsize = 50;
	// Use this for initialization
	void Start(){
		initialfontsize = DamageText.fontSize;
	}
	void OnEnable () {
		DamageText.fontSize = initialfontsize;
		transform.localScale = Vector3.one;
		TextAnim.SetTrigger("hit");
		Invoke("Deactivate", 1.1f);
	}
	public void setText(string damage, bool crit){
		if(crit){
			DamageText.fontSize = (int)(initialfontsize*1.5f);
			DamageText.fontStyle = FontStyle.BoldAndItalic;
		}else{	
			DamageText.fontSize = initialfontsize;
			DamageText.fontStyle = FontStyle.Italic;
		}
		if(Random.Range(0, 10)>5){
			if(Random.Range(0, 10)>5){
				DamageText.text = damage + " ";
			}else{
				DamageText.text = " " + damage;
			}
		}else{
			DamageText.text = damage;
		}
	}

	void Deactivate(){
		gameObject.SetActive(false);
	}
}
