using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class AdUIController : MonoBehaviour {
	public static AdUIController instance;
	public Sprite[] Icons;
	public Image Icon;
	public Text Title;
	public Text Description;
	public int Clicked = 0;
	public bool redeemed = false;
	public GameObject Panel;
	public GameObject RedeemButton;
	public GameObject YesButton;
	public GameObject NoButton;
	LanguageManager LM;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void OnEnable(){
		if(LM==null)
			LM = SmartLocalization.LanguageManager.Instance;
	}
	void Start () {
		LM = SmartLocalization.LanguageManager.Instance;
	}
	public void ClickYes(){
		Clicked = 1;
		Invoke("EnableRedeem", 2);
	}
	void EnableRedeem(){
		RedeemButton.SetActive(true);
		YesButton.SetActive(false);
		NoButton.SetActive(false);
	}
	public void ClickNo(){
		Clicked = 2;
		Hide();
	}
	public void Redeem(){
		redeemed = true;
		Hide();
	}
	public void Hide(){
		Panel.SetActive(false);
	}
	public void Show(){
		Panel.SetActive(true);
	}

	public void setReward(GiftController.RewardType r){
		Clicked = 0;
		redeemed = false;
		RedeemButton.SetActive(false);
		YesButton.SetActive(true);
		NoButton.SetActive(true);
		switch(r){
		case GiftController.RewardType.GOLD:
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Gold.Name");
			Description.text = LM.GetTextValue("Skill.Gold.Description");
			break;
		case GiftController.RewardType.FURY: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Fury.Name");
			Description.text = LM.GetTextValue("Skill.Fury.Description");
			break;
		case GiftController.RewardType.CLONE: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Clone.Name");
			Description.text = LM.GetTextValue("Skill.Clone.Description");
			break;
		case GiftController.RewardType.RAGE: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Helpersrage.Name");
			Description.text = LM.GetTextValue("Skill.Helpersrage.Description");
			break;
		case GiftController.RewardType.CRITICAL: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Critical.Name");
			Description.text = LM.GetTextValue("Skill.Critical.Description");
			break;
		case GiftController.RewardType.WRATH: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Naturewrath.Name");
			Description.text = LM.GetTextValue("Skill.Naturewrath.Description");
			break;
		case GiftController.RewardType.MONEY: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Moneyshot.Name");
			Description.text = LM.GetTextValue("Skill.Moneyshot.Description");
			break;
		case GiftController.RewardType.BEAN: 
			Icon.sprite = Icons[(int)r];
			Title.text = LM.GetTextValue("Skill.Bean.Name");
			Description.text = LM.GetTextValue("Skill.Bean.Description");
			break;
		}
	}
}
