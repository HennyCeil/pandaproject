﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoldTextController : MonoBehaviour {
	public Text GoldText;
	float t = 0;
	public Color OriginalColor;
	private Vector4 currentcolor;
	public float speed = 20.0f;

	void Update(){
		if(Time.time-t < 1.1f){
			if(Time.time-t >0.9f){
				currentcolor.w -= Time.deltaTime*5f;
				GoldText.color = (Color)currentcolor;
			}
			this.transform.position += (Vector3.up*Time.deltaTime*speed);
		}
	}
	void OnEnable () {
		GoldText.color = OriginalColor;
		currentcolor = OriginalColor;
		t = Time.time;
		Invoke("Deactivate", 1.1f);
	}
	public void setText(string gold, Vector2 pos){
		GoldText.text = gold;
		this.transform.position = pos;
	}
	
	void Deactivate(){
		gameObject.SetActive(false);
	}
}
