﻿using UnityEngine;
using System.Collections;

public class BeanSystemController : MonoBehaviour {

	public static BeanSystemController instance;
	public MagicBeanController[] Beans;
	private int current = 0;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		Beans = new MagicBeanController[transform.childCount];
		for(int i = 0; i < Beans.Length; i++){
			Beans[i] = transform.GetChild(i).GetComponent<MagicBeanController>();
		}
	}
	
	public void InstantiateBean(int value, Vector3 pos){
		Beans[current].value = value;
		Beans[current].pos = pos;
		Beans[current].gameObject.SetActive(true);
		current++;
		if(current == Beans.Length)
			current = 0;
	}
	public void InstantiateBean(int value){
		Beans[current].value = value;
		Beans[current].gameObject.SetActive(true);
		current++;
		if(current == Beans.Length)
			current = 0;
	}
}
