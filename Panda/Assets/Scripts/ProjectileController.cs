﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
	public Vector2 originalpos;
	public Rigidbody2D body;
	// Use this for initialization
	void OnEnable () {
		transform.position = originalpos;
		body.AddForce(new Vector2(1, 1));
	}
}
