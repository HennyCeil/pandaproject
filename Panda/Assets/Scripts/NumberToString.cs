﻿using UnityEngine;
using System.Collections;

public class NumberToString : MonoBehaviour {
	public static NumberToString instance;

	double[] convertion = new double[55];
	string[] scales = new string[55];
	void Awake(){
		instance = this;
		convertion[0] = 1000;
		for(int i=1; i<convertion.Length; i++){
			convertion[i] = convertion[i-1]*1000;
		}
		scales[0] = "";
		scales[1] = "K";
		scales[2] = "M";
		scales[3] = "B";
		scales[4] = "T";
		scales[5] = "aa";
		scales[6] = "bb";
		scales[7] = "cc";
		scales[8] = "dd";
		scales[9] = "ff";
		scales[10] = "gg";
		scales[11] = "hh";
		scales[12] = "ii";
		scales[13] = "jj";
		scales[14] = "kk";
		scales[15] = "ll";
		scales[16] = "mm";
		scales[17] = "nn";
		scales[18] = "oo";
		scales[19] = "pp";
		scales[20] = "qq";
		scales[21] = "rr";
		scales[22] = "ss";
		scales[23] = "tt";
		scales[24] = "uu";
		scales[25] = "vv";
		scales[26] = "ww";
		scales[27] = "xx";
		scales[28] = "yy";
		scales[29] = "zz";
		scales[30] = "AA";
		scales[31] = "BB";
		scales[32] = "CC";
		scales[33] = "DD";
		scales[34] = "EE";
		scales[35] = "FF";
		scales[36] = "GG";
		scales[37] = "HH";
		scales[38] = "II";
		scales[39] = "JJ";
		scales[40] = "KK";
		scales[41] = "LL";
		scales[42] = "MM";
		scales[43] = "NN";
		scales[44] = "OO";
		scales[45] = "PP";
		scales[46] = "QQ";
		scales[47] = "RR";
		scales[48] = "SS";
		scales[49] = "TT";
		scales[50] = "UU";
		scales[51] = "VV";
		scales[52] = "XX";
		scales[53] = "YY";
		scales[54] = "ZZ";
	}
	void Start(){


	}
	public string Convert(double n){
		if(n<0)
			n=0;
		for(int i=0; i<convertion.Length; i++){
			if(n < convertion[i]){
				if(i==0)
					return n.ToString("F0");
				else
					return ((n/convertion[i-1]).ToString("F2") + scales[i]);
			}
		}
		return n.ToString("F3");
	}
}
