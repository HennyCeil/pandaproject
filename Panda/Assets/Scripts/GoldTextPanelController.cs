﻿using UnityEngine;
using System.Collections;

public class GoldTextPanelController : MonoBehaviour {

	public static GoldTextPanelController instance;
	public GoldTextController[] TextGold;
	private int current = 0;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		TextGold = new GoldTextController[transform.childCount];
		for(int i = 0; i < TextGold.Length; i++){
			TextGold[i] = transform.GetChild(i).GetComponent<GoldTextController>();
		}
	}
	
	public void InstantiateGoldText(string value, Vector2 pos){
		TextGold[current].setText(value, pos);
		TextGold[current].gameObject.SetActive(true);
		current++;
		if(current == TextGold.Length)
			current = 0;
	}
}
