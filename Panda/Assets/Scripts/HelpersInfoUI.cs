﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class HelpersInfoUI : MonoBehaviour {
	public static HelpersInfoUI instance;
	public GameObject Panel;
	public Image Back;
	public HelperUIPasiveSkillController [] HelpersSkills;
	public Text IconName;
	public Text Level;
	public Text DPS;
	public Text Description;
	public Image Icon;
	LanguageManager LM;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		LM = LanguageManager.Instance;
	}
	
	// Update is called once per frame
	public void Show () {
		Panel.SetActive(true);
	}
	public void Hide(){
		Panel.SetActive(false);
	}
	public void SetWindow(string id, int lvl, double dps){
		SetDescription(LM.GetTextValue(id + ".Description"));
		SetName(LM.GetTextValue(id + ".Name"));
		SetLevel(lvl);
		SetDPS(dps);
	}
	public void SetIcon(Sprite img, bool reset){
		if(reset)
			Back.enabled = true;
		else
			Back.enabled = false;
		Icon.sprite = img;
	}
	public void SetName(string n){
		IconName.text = n;
	}
	public void SetLevel(int l){
		Level.text = LM.GetTextValue("Game.Levelabb")+": " + l;
	}
	public void SetDPS(double d){
		DPS.text = LM.GetTextValue("Game.DPSabb")+": " + NumberToString.instance.Convert(d);
	}
	public void SetDescription(string d){
		Description.text = d;
	}
	public void SetSkills(HelperClass.HelperSkillType[] skills, bool[] skillactive){
		for(int i=0; i<HelpersSkills.Length; i++){
			HelpersSkills[i].SetSkillName(skills[i].ToString(), skillactive[i]);
		}
	}
}
