﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class HelperClass : MonoBehaviour {
	public enum HelperSkillType{
		DamageThis,
		DamageBoss,
		DamageAll,
		DamageTap,
		DamageTapFromDPS,
		GoldAll,
		GoldMonster,
		GoldCage,
		GoldBoss,
		CritChance,
		CritDamage,
		CritChanceOnCD,
		CritDamageBoss,
		Reset
	}
	public enum HelperType{
		MACAW,
		TURTLE,
		KOALA,
		PENGUIN,
		ORANGUTAN,
		GORILLA,
		TIGER,
		CROCODILE,
		ELEPHANT
	}
	private string[] HelperKey = {"Macaw", "Turtle", "Koala", "Penguin", "Orangutan", "Gorilla", "Tiger", "Crocodile", "Elephant"};
	//UIElements
	public HelperType HelperID;
	public Text TitleText;
	public Text LevelText;
	public Image Icon;
	public Image Back;
	public Image IncrementalButton;
	public Text IncrementalText;
	public Text nextPrice;
	public Text nextDamage;
	public Text DPSText;
	public Image[] Medals;

	//Helper Object, animator
	public GameObject HelperObject;
	public Animator HelperAnim;
	public GameObject HelperLevelUp;

	//HelperStats
	public HelperSkillType[] Skills;
	public bool [] SkillIsActive;
	public double [] SkillValue;
	private int currentLevel = 0;
	public double currentDPS;
	private double basedamage;
	private double basecost;
	private double currentcost;
	private double nextd;
	public float speed = 1;
	public string IdHelper;
	public bool reset = false;
	public float HelperAnimSpeed = 1f;
	private LanguageManager LM;
	private string HelperName;
	private string HelperDescription;
	public bool Unlocked = false;
	private bool Dead = false;
	private int skilltounlock = 0;

	public GameObject plus10;
	public GameObject plus100;
	
	private bool plus10active = false;
	private bool plus100active = false;
	private float plus10sec = 0;
	private float plus100sec = 0;

	private MonstersController MonsterC;

	void Awake(){
		IdHelper = "Helpers." + HelperKey[(int)HelperID];
		SkillIsActive = new bool[10];
		for(int i=0; i<SkillIsActive.Length; i++){
			SkillIsActive[i] = JuicePrefs.GetInt(IdHelper+"skill"+i.ToString(), 0)==1;
		}

		HelperLevelUp.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		Invoke("InitHelper", 0.1f);
		LM = LanguageManager.Instance;
		LM.OnChangeLanguage += LanguageSetup;
		LanguageSetup(LM);
		MonsterC = MonstersController.instance;
		if(reset){
			Back.enabled = true;
		}else{
			Back.enabled = false;
		}
	}
	public void InitHelper(){
		//load()
		basecost = PandaMath.HelpersBaseCost[(int)HelperID];
		basedamage = basecost * PandaMath.HelpersBaseDamage[(int)HelperID];
		currentcost = JuicePrefs.GetDouble(IdHelper+"currentcost", basecost);
		currentDPS = JuicePrefs.GetDouble(IdHelper+"currentDPS",0);
		currentLevel = JuicePrefs.GetInt(IdHelper+"currentLevel",0);
		skilltounlock = JuicePrefs.GetInt(IdHelper+"skilltounlock",0);
		nextd = PandaMath.GetUpgradeDamage(currentLevel, basedamage)*PandaMath.DecreasedEfectiveness(currentLevel);
		if(nextd<1)
			nextd = basedamage;
		nextPrice.text = NumberToString.instance.Convert(currentcost);
		nextDamage.text = "+"+NumberToString.instance.Convert(basedamage);
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		DPSText.text = NumberToString.instance.Convert(currentDPS)+ "  "  +  LM.GetTextValue("Game.DPSabb");
		for(int i=0; i<10; i++){
			Medals[i].enabled = false;
			if(SkillIsActive[i]){
				Medals[i].enabled = true;
			}
		}
		Unlocked = JuicePrefs.GetInt(IdHelper+"Unlocked", 0)==1;
		HelperObject.SetActive(Unlocked);
		StartCoroutine(Damage());
	}
	// Update is called once per frame
	void FixedUpdate () {
		LevelText.text = LM.GetTextValue("Game.Levelabb") + ": " + currentLevel.ToString();
		DPSText.text = NumberToString.instance.Convert(currentDPS)+ "  "  +  LM.GetTextValue("Game.DPSabb");
		if(GoldSystemController.instance.GetGold()<currentcost){
			IncrementalButton.color = Color.gray;
		}else{
			if(!Unlocked || currentLevel==0 || currentLevel==10 || currentLevel==25 || currentLevel==50 || currentLevel==100 || currentLevel==200 || currentLevel==400 || currentLevel==600 || currentLevel==1000 || currentLevel==1500)
				IncrementalButton.color = Color.yellow;
			else
				IncrementalButton.color = GameManager.instance.activecolor;
		}
		if(!Unlocked || currentLevel==0 || currentLevel==10 || currentLevel==25 || currentLevel==50 || currentLevel==100 || currentLevel==200 || currentLevel==400 || currentLevel==600 || currentLevel==1000 || currentLevel==1500)
			IncrementalText.text = LM.GetTextValue("Game.Unlock");
		else
			IncrementalText.text = LM.GetTextValue("Game.LevelUp");

		for(int i=0; i<10; i++){
			if(SkillIsActive[i]){
				Medals[i].enabled = true;
				JuicePrefs.SetInt(IdHelper+"skill"+i.ToString(), 1);
			}
		}
	}
	public void IncreaseHelperLevel(){
		if(GoldSystemController.instance.GetGold()>=currentcost){
			if (HelperLevelUp.activeSelf)
				HelperLevelUp.SetActive (false);
			HelperLevelUp.SetActive (true);
			HelperLevelUp.GetComponent<AudioSource>().Play();

			if(GoldSystemController.instance.GetGold()>=currentcost*15 && !plus10active){
				plus10sec = 3f;
				StartCoroutine(ActivatePlus10());
			}
			if(GoldSystemController.instance.GetGold()>=currentcost*150 && !plus100active){
				plus100sec = 3f;
				StartCoroutine(ActivatePlus100());
			}
			AchievementsController.instance.UpdateLevels();
			double prev = nextd;
			if(!Unlocked){
				Unlocked = true;
				HelperObject.SetActive(true);
				JuicePrefs.SetInt(IdHelper+"Unlocked", 1);
			}
			GoldSystemController.instance.SubstractGoldInmediate(currentcost);
			currentDPS += nextd;
			if(currentLevel==10 || currentLevel==25 || currentLevel==50 || currentLevel==100 || currentLevel==200 || currentLevel==400 || currentLevel==600 || currentLevel==1000 || currentLevel==1500){
				SkillIsActive[skilltounlock] = true;
				if(Skills[skilltounlock] == HelperSkillType.DamageThis){
					currentDPS*= SkillValue[skilltounlock];
					prev *= SkillValue[skilltounlock];
				}
				skilltounlock++;
			}
			currentLevel++;
			currentcost = PandaMath.GetUpgradeCost(currentLevel,basecost)*PandaMath.IncreasedEfectiveness(currentLevel)-currentDPS;
			if(currentLevel==10 || currentLevel==25 || currentLevel==50 || currentLevel==100 || currentLevel==200 || currentLevel==400 || currentLevel==600 || currentLevel==1000 || currentLevel==1500){
				currentcost*=10;
			}
			nextd = (PandaMath.GetUpgradeDamage(currentLevel, basedamage));
			if(nextd < prev*0.12d)
				nextd += prev*0.12d*PandaMath.DecreasedEfectiveness(currentLevel);
			if(nextd <1){
				nextd = 1;
			}
			LevelText.text = "Lvl: " + currentLevel.ToString();
			nextPrice.text = NumberToString.instance.Convert(currentcost);
			nextDamage.text ="+" + NumberToString.instance.Convert(nextd) + LM.GetTextValue("Game.DMGabb");
			JuicePrefs.SetDouble(IdHelper+"currentcost", currentcost);
			JuicePrefs.SetDouble(IdHelper+"currentDPS",currentDPS);
			JuicePrefs.SetInt(IdHelper+"currentLevel",currentLevel);
			JuicePrefs.SetDouble(IdHelper+"nextd",nextd);
			JuicePrefs.SetInt(IdHelper+"skilltounlock",skilltounlock);

		}
	}
	public void OpenInfo(){
		HelpersInfoUI.instance.SetSkills(Skills, SkillIsActive);
		HelpersInfoUI.instance.SetIcon(Icon.sprite, reset);
		HelpersInfoUI.instance.SetWindow(IdHelper, currentLevel, currentDPS);
		HelpersInfoUI.instance.Show();
	}
	public void LanguageSetup(LanguageManager langm){
		HelperName = langm.GetTextValue(IdHelper + ".Name");
		HelperDescription = langm.GetTextValue(IdHelper + ".Name");
		TitleText.text = HelperName;
	}
	public void UnlockHelper(){
		Unlocked = true;
	}
	public void ResetHelper(){
		reset = true;
	}
	IEnumerator Damage(){
		while(true){
			if(Unlocked && !Dead){
				if(HelperAnim!=null && HelperAnim.isActiveAndEnabled)HelperAnim.SetTrigger("shoot");
				Invoke("DoDamage", HelperAnimSpeed);
			}
			yield return new WaitForSeconds(speed/SkillsManager.instance.GetHelpersSpeed() * (PandaMath.HelperGlobalSpeed));
		}
	}
	void DoDamage(){
		MonstersController.instance.DamageMonster(currentDPS * PandaMath.DamageAllBonus() * speed * PandaMath.HelperGlobalSpeed, false);

	}

	IEnumerator ActivatePlus10(){
		plus10active = true;
		plus10.SetActive(true);
		while(plus10sec>0){
			plus10sec -= 0.5f;
			yield return new WaitForSeconds(0.5f);
		}
		plus10active = false;
		plus10.SetActive(false);
	}
	IEnumerator ActivatePlus100(){
		plus100active = true;
		plus100.SetActive(true);
		while(plus100sec>0){
			plus100sec -= 0.5f;
			yield return new WaitForSeconds(0.5f);
		}
		plus100active = false;
		plus100.SetActive(false);
	}

	public void Increase10(){
		for(int i=0; i<10; i++){
			IncreaseHelperLevel();
		}
	}
	public void Increase100(){
		for(int i=0; i<100; i++){
			IncreaseHelperLevel();
		}
	}
}
