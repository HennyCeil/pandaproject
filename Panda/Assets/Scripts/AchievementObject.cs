﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementObject : MonoBehaviour {
	int currstep;
	public double[] valuestobeat;
	bool[] claimed;
	public Text title;
	public string defaulttext;
	public Image button;
	string key;
	// Use this for initialization
	void Awake(){
		defaulttext = title.text;
		claimed = new bool[valuestobeat.Length];
	}
	void Start () {
		for(int i=0; i<claimed.Length; i++)
			claimed[i] = false;
		key = defaulttext.Replace(" ", "").Replace("%n", "");
		for(int i=0; i<claimed.Length; i++)
			claimed[i] = JuicePrefs.GetInt(key+i.ToString(), -1)>=i;
	}
	
	// Update is called once per frame
	public void UpdateAchievement (double v) {
		currstep = -1;
		for(int i=0; i<valuestobeat.Length; i++){
			if(v >= valuestobeat[i])
				currstep = i;
		}
		if(currstep<0){
			button.color = Color.grey;
		}else{
			if(claimed[currstep]){
				button.color = Color.grey;
			}else{
				button.color = Color.yellow;
			}
		}
		if(currstep>=0 && currstep<valuestobeat.Length){
			if(currstep+1<valuestobeat.Length && claimed[currstep])
				title.text = defaulttext.Replace("%n", NumberToString.instance.Convert(valuestobeat[currstep+1]));
			if(currstep+1<valuestobeat.Length && !claimed[currstep])
				title.text = defaulttext.Replace("%n", NumberToString.instance.Convert(valuestobeat[currstep]));
		}
	}
	public void ClaimReward(){
		if(claimed[currstep]) return;
		claimed[currstep] = true;
		JuicePrefs.SetInt(key+currstep.ToString(),currstep);
		MagicBeanSystemController.instance.AddBeans(20);
	}
}
