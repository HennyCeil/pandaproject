﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

public class SplashScriptController : MonoBehaviour {
	public static SplashScriptController instance;
	void Awake(){
		ZPlayerPrefs.useSecure = true;
		ZPlayerPrefs.Initialize("panda2015", "2cbff7de09b5beca3e5764d0227e0947");
		instance = this;
	}

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this.gameObject);
		SoomlaStore.Initialize(new PandaAssets());
		Invoke("LoadMain", 5);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void LoadMain(){
		Application.LoadLevel(1);
	}
}
