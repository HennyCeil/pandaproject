﻿using UnityEngine;
using System.Collections;

public class SkillsManager : MonoBehaviour {
	public static SkillsManager instance;
	public float helpersSpeed = 1;
	public float critchanceskill = 0;
	public float wrathdamaebonus = 1;
	public bool MoneyShotActive = false;
	public float MoneShotValue = 0;
	public SkillClass[] Skills;
	// Use this for initialization
	void Awake () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public float GetHelpersSpeed(){
		return helpersSpeed;
	}
	public float GetCritChanceSkill(){
		return critchanceskill;
	}
	public SkillClass GetSkill(int i){
		return Skills[i];
	}
}
