﻿/*
 * Copyright (C) 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public enum PandaAchievement {
	achievementMonsterSlayer,
	achievementTapTapTap,
	achievementDefendersAssemble,
	achievementVeggieLover,
	achievementEpicForestDefender,

}

public class SocialFeatureManager : MonoBehaviour 
{
	private static SocialFeatureManager current;
	
	private int score;
	private static string [] PandaAchievements = 
	{
		"CgkIicrl5ecTEAIQAg",
		"CgkIicrl5ecTEAIQAw",
		"CgkIicrl5ecTEAIQBA",
		"CgkIicrl5ecTEAIQBQ",
		"CgkIicrl5ecTEAIQBg"
	};
	private static string [] PandaAchievementsNames = 
	{
		"Monster Slayer",
		"Tap! Tap! Tap!",
		"Defenders Assemble",
		"Veggie Lover",
		"Epic Forest Defender",
	};

	void Start () 
	{
		//PlayerPrefs.DeleteAll();
		//PlayerPrefs.Save();
		score = JuicePrefs.GetInt("Game.MaxLevel", 0);
		current = this;
		DontDestroyOnLoad(this);
		
		// Select the Google Play Games platform as our social platform implementation
		#if UNITY_ANDROID
		GooglePlayGames.PlayGamesPlatform.Activate();
		#endif
		AuthenticateUser();
		
	}
	
	public static void AuthenticateUser()
	{
		Social.localUser.Authenticate((bool success) => {
			Debug.Log ( success ? "Successfully authenticated" : "Authentication failed.");
		});
	}
	
	public static void LogOutUser()
	{
		#if UNITY_ANDROID
		PlayGamesPlatform.Instance.SignOut();
		#else
		Debug.Log("Log Out.");
		#endif
	}
	
	public static void UnlockAchievement(PandaAchievement a)
	{
#if UNITY_ANDROID || UNITY_IOS
		if(isAuthenticated()){
		Social.ReportProgress(PandaAchievements[(int) a], 100.0f, (bool success) => {
			if(success)
			{
				Debug.Log("success");
			}
			else
			{
				Debug.Log("failed");
			}
		});
		}
#endif
	}
	
	public static void PostToLeaderBoard(int s)
	{
#if UNITY_ANDROID || UNITY_IOS
		if(isAuthenticated()){
			Social.ReportScore(s, "CgkIicrl5ecTEAIQAQ", (bool success) => {
				if(success)
				{
					Debug.Log("success");
				}
				else
				{
					Debug.Log("failed");
				}
			});
		}
		#endif
	}
	
	public static void ShowLeaderBoard()
	{
		if(isAuthenticated()){
		#if UNITY_ANDROID
			PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIicrl5ecTEAIQAQ");
		#else
			Social.Active.ShowLeaderboardUI();
		#endif
			
			//GA.API.Design.NewEvent("GAME:SHOWLEADERBOARD:",1);
		}
	}

	
	public static void ShowAchievementsUI()
	{
		if(isAuthenticated()){
			Social.Active.ShowAchievementsUI();
			//GA.API.Design.NewEvent("GAME:SHOWACHIEVEMENTS:",1);
		}
	}
	

	public static bool isAuthenticated()
	{
		return Social.localUser.authenticated;
	}
	
}