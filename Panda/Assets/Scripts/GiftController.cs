﻿using UnityEngine;
using System.Collections;

public class GiftController : MonoBehaviour {
	public enum RewardType{
		GOLD=0,
		FURY,
		CLONE,
		RAGE,
		CRITICAL,
		WRATH,
		MONEY,
		BEAN
	}
	RewardType mRewardType;
	bool clicked = false;
	bool opened = false;
	public AdUIController AdUICtrl;
	// Use this for initialization
	void Start () {
		AdUICtrl = AdUIController.instance;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		if(!clicked){
			AchievementsController.instance.UpdateBirds();
			clicked = true;
			this.transform.parent.GetComponent<TucanController>().GetOut();
			this.transform.parent = null;
			this.transform.localRotation = Quaternion.identity;
			GetComponent<DistanceJoint2D>().enabled = false;
		}
	}
	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.layer == this.gameObject.layer){
			if(!opened){
				GetComponent<CircleCollider2D>().enabled = false;
				GetComponent<Rigidbody2D>().isKinematic = true;
				this.GetComponent<Animator>().SetTrigger("open");

				StartCoroutine(SkillSelector());
				opened = true;
			}
		}
	}
	
	IEnumerator SkillSelector(){
		mRewardType = (RewardType)(Random.Range((int)RewardType.GOLD, (int)RewardType.BEAN+1));
		if((int)mRewardType > 0 && mRewardType != RewardType.BEAN){
			if(!SkillsManager.instance.GetSkill(((int)mRewardType)-1).Available){
				mRewardType = RewardType.GOLD;
			}
		}
		yield return new WaitForEndOfFrame();
		if(Supersonic.Agent.isRewardedVideoAvailable()){
			//ad ready
			//PauseGame();
			AdUICtrl.Show();
			AdUICtrl.setReward(mRewardType);
			while(true){
				if(AdUICtrl.Clicked==1){
					//showad()
					//wait for return
					GameManager.instance.showad();
					while(!AdUICtrl.redeemed){
						yield return new WaitForFixedUpdate();
					}
					Reward(true);
					break;
				}
				if(AdUICtrl.Clicked==2){
					AdUICtrl.Hide();
					break;
				}
				yield return new WaitForFixedUpdate();
			}
		}else{
			Reward(false);
		}
	}
	void Reward(bool ad){
		AchievementsController.instance.UpdateOpenseed();
		int l = 1;
		if(ad){
			l=5;
		}
		switch(mRewardType){
		case RewardType.GOLD:
			GoldGift(5*l);
			break;
		case RewardType.FURY: 
			SkillGift(0);
			break;
		case RewardType.CLONE: 
			SkillGift(1);
			break;
		case RewardType.RAGE: 
			SkillGift(2);
			break;
		case RewardType.CRITICAL: 
			SkillGift(3);
			break;
		case RewardType.WRATH: 
			SkillGift(4);
			break;
		case RewardType.MONEY: 
			SkillGift(5);
			break;
		case RewardType.BEAN: 
			BeansGift(l);
			break;
		}
		DestroyObject(this.gameObject, 4);
	}
	void GoldGift(int n){
		double g = System.Math.Floor(PandaMath.GetFibonacciForLevel(MonstersController.instance.level, 1, 1, 1,1)*PandaMath.GetGoldBonus(false, false));
		if(g<1) g=1;
		g*=n;
		g = g*HelpersController.instance.GetGoldAllBonus();
		CoinSystemController.instance.SplitInCoins(g, transform.position);
	}
	void BeansGift(int n){
		for(int i=0; i<n; i++)
			BeanSystemController.instance.InstantiateBean(1, transform.position);
	}
	void SkillGift(int i){
		SkillsManager.instance.GetSkill(i).ActivateSkillGift();
	}

}
