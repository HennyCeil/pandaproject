﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class HelperBarController : MonoBehaviour {

	public enum HelperType{
		MACAW,
		ELEPHANT,
		PENGUIN,
		KOALA,
		ORANGUTAN,
		TURTLE,
		TIGER,
		CROCODILE,
		GORILLA
	}
	private string[] HelperKey = {"Macaw", "Elephant", "Penguin", "Koala", "Orangutan", "Turtle", "Tiger", "Crocodile", "Gorilla"};
	public HelperType HelperID;
	public Text Title;
	public Text Description;
	public Image Background;
	public Image IncrementalButton;
	public Text IncrementalText;


	private string IdHelper;
	private LanguageManager LangManager;
	public bool UseIncrementalButton = true;
	// Use this for initialization
	void Start () {
		IdHelper = "Helpers." + HelperKey[(int)HelperID];
		LangManager = LanguageManager.Instance;
		Title.text = LangManager.GetTextValue(IdHelper + ".Name");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public string GetID(){
		return IdHelper;
	}
}
