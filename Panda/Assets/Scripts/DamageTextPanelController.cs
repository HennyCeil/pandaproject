﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamageTextPanelController : MonoBehaviour {
	public static DamageTextPanelController instance;
	public DamageTextController[] TextDamage;
	private int current = 1;
	// Use this for initialization
	void Awake(){
		instance = this;
	}
	void Start () {
		TextDamage = new DamageTextController[transform.childCount];
		for(int i = 0; i < TextDamage.Length; i++){
			TextDamage[i] = transform.GetChild(i).GetComponent<DamageTextController>();
		}
	}

	public void InstantiateDamageText(string damage, bool crit = false){
		TextDamage[current].gameObject.SetActive(true);
		TextDamage[current].setText(damage, crit);
		current++;
		if(current == TextDamage.Length)
			current = 1;
	}
	public void InstanciateFuryDamage(string damage, bool crit = false){
		TextDamage[0].gameObject.SetActive(true);
		TextDamage[0].setText(damage, crit);
	}
}
