﻿using UnityEngine;
using System.Collections;

public class TucanController : MonoBehaviour {
	Vector3 finalpos;
	bool away = false;
	// Use this for initialization
	void Start(){
		Invoke("GetOut", 30);
	}
	void Update(){
		if(away){
			transform.position = Vector3.Slerp(transform.position, finalpos, Time.deltaTime*0.33f);
		}
	}

	public void GetOut () {
		this.gameObject.GetComponent<Animator>().enabled = false;
		finalpos = new Vector3(transform.position.x,transform.position.y+15f,transform.position.z); 
		away = true;
		DestroyObject(this.gameObject, 4);
	}
}
