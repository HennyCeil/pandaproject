﻿using UnityEngine;
using System.Collections;

public class JuicePrefsTests : MonoBehaviour {

	// Use this for initialization
	void Start () {

		//int
		JuicePrefs.SetInt ("Life",123);
		Debug.Log ("Int: " + JuicePrefs.GetInt ("Life"));

		//string
		JuicePrefs.SetString ("Name","Hello World!!");
		Debug.Log ("String: " + JuicePrefs.GetString ("Name"));
	
		//float
		JuicePrefs.SetFloat ("Stamina",123f);
		Debug.Log ("Float: " + JuicePrefs.GetFloat ("Stamina"));	

		//double
		JuicePrefs.SetDouble ("Accuracy",123.456);
		Debug.Log ("Double: " + JuicePrefs.GetDouble ("Accuracy"));

		//timestamp
		Debug.Log ("Timestamp: " + JuicePrefs.GetTimeStamp());

		//get file
		Debug.Log ("File: " + JuicePrefs.getFile());

		//set file
		JuicePrefs.setFile ("{\"timeStamp\":0.1,\"Life\":321,\"Name\":\"Bye Bye World!!\",\"Stamina\":321,\"Accuracy\":321.654}");
		Debug.Log ("New File: " + JuicePrefs.getFile());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
