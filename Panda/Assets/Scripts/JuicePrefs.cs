﻿using UnityEngine;
using System.Collections;
using MiniJSON;
using System.Collections.Generic;
using System.IO;

/*
 * Singleton 
 */
public class JuicePrefs {
	
	Dictionary<string,object> JSONObject;
	string JSONFile;
	private string filePath = "PandaConfig";
	
	/*
	 * Singleton instance
	 */
	private static JuicePrefs singleInstance = null;
	
	/*
	 * Not instantiable by outsiders.
	 */
	private JuicePrefs(){
	}
	
	/*
	 * Singleton instance getter
	 */
	private static JuicePrefs getInstance(){
		if (singleInstance == null) {
			singleInstance = new JuicePrefs();
			singleInstance.initFile();
		}
		
		return singleInstance;
	}
	
	public static string getFile(){
		return getInstance ().JSONFile;
	}	
	
	public static void setFile(string file){
		getInstance ().JSONFile = file;
		getInstance ().JSONObject = Deserialize ();
		getInstance ().saveFile ();
	}	
	
	//--------------- Setters ---------------
	
	public static void SetTimeStamp(){
		getInstance().saveFile ();
	}
	
	public static void SetInt(string key, int value){
		getInstance ().JSONObject [key] = value.ToString();
		getInstance().saveFile ();
	}
	
	public static void SetFloat(string key, float value){
		getInstance ().JSONObject [key] = value.ToString();
		getInstance().saveFile ();
	}
	
	public static void SetDouble(string key, double value){
		getInstance ().JSONObject [key] = value.ToString();
		getInstance().saveFile ();
	}
	
	public static void SetString(string key, string value){
		getInstance ().JSONObject [key] = value;
		getInstance().saveFile ();
	}
	
	//--------------- Getters ---------------
	
	public static double GetTimeStamp(){
		return (double)getInstance().JSONObject["timeStamp"];
	}
	/*
	public static int GetInt(string key){
		if (getInstance ().JSONObject.ContainsKey (key))
			return (int)getInstance ().JSONObject [key];
		else
			return 0;
	}
	
	public static float GetFloat(string key){
		if (getInstance ().JSONObject.ContainsKey (key))
			return (float)getInstance ().JSONObject [key];
		else
			return 0f;
	}
	
	public static double GetDouble(string key){
		if (getInstance ().JSONObject.ContainsKey (key))
			return (double)getInstance ().JSONObject [key];
		else
			return 0.0;
	}
	
	public static string GetString(string key){
		if (getInstance ().JSONObject.ContainsKey (key))
			return (string)getInstance ().JSONObject [key];
		else
			return "";
	}*/
	public static int GetInt(string key, int def = 0){
		if (getInstance ().JSONObject.ContainsKey (key)) {
			int ret = 0;
			if (int.TryParse((string)getInstance ().JSONObject [key], out ret))
				return ret;
			else
				return def;
		}
		else
			return def;
	}
	
	public static float GetFloat(string key, float def = 0f){
		if (getInstance ().JSONObject.ContainsKey (key)){
			float ret = 0;
			if (float.TryParse((string)getInstance ().JSONObject [key], out ret))
				return ret;
			else
				return def;
		}
		else
			return def;
	}
	
	public static double GetDouble(string key, double def = 0.0){
		if (getInstance ().JSONObject.ContainsKey (key)){
			double ret = 0;
			if (double.TryParse((string)getInstance ().JSONObject [key], out ret))
				return ret;
			else
				return def;
		}
		else
			return def;
	}
	
	public static string GetString(string key, string def = ""){
		if (getInstance ().JSONObject.ContainsKey (key))
			return (string)getInstance ().JSONObject [key];
		else
			return def;
	}
	//--------------- Internal ------------------
	
	private double getTime(){
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
		return (System.DateTime.UtcNow - epochStart).TotalSeconds;
	}
	
	private static Dictionary<string,object> Deserialize()
	{
		return (Dictionary<string,object>) MiniJSON.Json.Deserialize(getInstance().JSONFile);
	}
	
	private static string Serialize()
	{
		return MiniJSON.Json.Serialize(getInstance().JSONObject);
	}
	
	void initFile(){
		string fileText = loadFile ();
		if (fileText != "") {
			JSONFile = fileText;
			JSONObject = Deserialize();
		} else {
			JSONFile =  "{ \"timeStamp\": " + getTime() + " }";
			JSONObject = Deserialize();
			saveFile();
		}
	}
	
	string loadFile(){
		return ZPlayerPrefs.GetString (getInstance().filePath);
	}
	
	void saveFile(){
		getInstance ().JSONObject ["timeStamp"] = getTime ();
		getInstance ().JSONFile = Serialize ();
		ZPlayerPrefs.SetString (filePath, getInstance().JSONFile);
	}
}
