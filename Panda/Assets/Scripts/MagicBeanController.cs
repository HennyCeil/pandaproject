﻿using UnityEngine;
using System.Collections;

public class MagicBeanController : MonoBehaviour {
	bool clicked = false;
	public int value;
	public GameObject Part;
	public Vector3 pos;
	// Use this for initialization
	void OnEnable () {
		clicked = false;
		Invoke("AddBean", Random.Range(3.5f, 4.5f));
		if(pos!=Vector3.zero){
			transform.position = pos;
			GetComponent<Rigidbody2D>().AddForce(new Vector2(-Random.Range(-50f, 50f), Random.Range(20f, 110f)), ForceMode2D.Impulse);
		}else{
			transform.localPosition = Vector2.one*3f;
			GetComponent<Rigidbody2D>().AddForce(new Vector2(-Random.Range(5f, 110f), Random.Range(20f, 110f)), ForceMode2D.Impulse);
		}
		pos = Vector3.zero;
	}

	// Update is called once per frame
	void AddBean () {
		CancelInvoke("AddBean");
		Object p = Instantiate(Part, transform.position, Quaternion.identity);// Part.SetActive(true);
		Destroy(p, 1f);
		gameObject.SetActive(false);
		MagicBeanSystemController.instance.AddBeans(value);
		value = 0;
	}
	void OnMouseDown() {
		if(clicked)
			return;
		clicked = true;
		AddBean();
	}
}
