﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonstersController : MonoBehaviour {
	struct DamageSource{
		public double value;
		public bool visible;
	}
	public enum TypeOfMonster{
		Normal=0,
		Cage,
		MiniBoss,
		Boss
	}
	public ParticleSystem poof;
	public static MonstersController instance;
	public Animator CurrentMonsterAnim;
	private DamageTextPanelController DamageTextPC;
	public GameObject[] Monsters;
	public int currentMonster;
	public int monsters = 0;
	public int maxMonsters = 10;
	public int monstersKilled = 0;
	public int totalMonstersKilled = 0;
	public int bosses = 0;

	private double Health = 15;
	public double maxHealth = 15;
	public double currentMaxHealth = 15;
	public bool isDead = false;
	public int level = 0;
	private readonly object syncLock = new object();
	public bool onBoss = false;
	public TypeOfMonster mType;
	private Queue<DamageSource> DamageQ;
	private DamageSource dmg;
	private bool monsterable = true;
	private bool monsterhitable = true;

	public int oldBoss = 1;
	public bool retrieve_boss = false;
	// Use this for initialization
	void Awake(){
		instance = this;
		maxHealth = JuicePrefs.GetDouble("Monster_maxhealth", maxHealth);
		currentMaxHealth = JuicePrefs.GetDouble("Monster_currentMaxHealth", currentMaxHealth);
		Health = JuicePrefs.GetDouble("Monster_Health", Health);
		level = JuicePrefs.GetInt("Monster_level", level);
	}
	void Start(){
		mType = TypeOfMonster.Normal;
		DamageQ = new Queue<DamageSource>();
		DamageTextPC = DamageTextPanelController.instance;
		NextMonster (false);
		SetMonsterHP();
	}

	public void NextMonster (bool boss) {
		Monsters[currentMonster].transform.localScale = Vector3.one;
		Monsters[currentMonster].SetActive(false);
		currentMonster = Random.Range(0, 74);
		if (currentMonster > 74)
			currentMonster = 74;
		if(!boss && Random.Range(0f, 1f)<PandaMath.CageChace){
			currentMonster = 75;
			mType = TypeOfMonster.Cage;
		}else{
			if(!boss){
				mType = TypeOfMonster.Normal;
			}
		}
		if (boss) {
			if(retrieve_boss)
			{
				currentMonster = oldBoss;
				retrieve_boss = false;
			}
			if(currentMonster==1 || (currentMonster>=20 && currentMonster<=23))
				Monsters [currentMonster].transform.localScale = Vector3.one * 1.46f;
			else
				Monsters [currentMonster].transform.localScale = Vector3.one * 1.66f;
		}
		CurrentMonsterAnim = Monsters[currentMonster].GetComponent<Animator>();
		if(monsterable)
			Monsters[currentMonster].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		while(DamageQ.Count>0 && !isDead){
			lock(syncLock){
				dmg = DamageQ.Dequeue();
				if(mType == TypeOfMonster.Cage)
					dmg.value/=2d;
				if(mType == TypeOfMonster.Boss || mType == TypeOfMonster.MiniBoss )
					dmg.value*=PandaMath.CurrentDamageOnBoss();
				Health -= dmg.value;
				if(dmg.visible){
					if(DamageTextPC==null)
						DamageTextPC = DamageTextPanelController.instance;
					if(DamageTextPC!=null)
						DamageTextPC.InstantiateDamageText(NumberToString.instance.Convert(dmg.value));
				}
			}
			if(Health<=0d){
				Health = 0;
				DamageQ.Clear();
				return;
			}
		}
	}
	public double GetMonsterHP(){
		return Health;
	}
	public void DamageMonster(double damage, bool visible = true){
		if(!monsterable) return;
		lock(syncLock){
			DamageSource d;
			d.value = damage;
			d.visible = visible;
			DamageQ.Enqueue(d);
		}
	}
	public void SetMonsterHP(){
		double factor = 1.0d;
		if(level >0)
			factor = factor + Mathf.Log10(level);
		maxHealth = PandaMath.GetFibonacciForLevel(level-1, 0.98d, 0.95d, PandaMath.initialHPMonster, PandaMath.secondHPMonster)*PandaMath.IncreasedEfectiveness(level)/(factor);
		Health = maxHealth;
		currentMaxHealth = maxHealth;
		int r =1;
		if(level>1)
			r = 2*(level%5);
		if(r==8) r=7;
		switch(mType){
		case TypeOfMonster.Normal: NextMonster(false);break;
		case TypeOfMonster.Cage: NextMonster(false);break;
		case TypeOfMonster.MiniBoss: Health*=r;currentMaxHealth*=r;NextMonster(true);break;
		case TypeOfMonster.Boss: Health*=10;currentMaxHealth*=10;NextMonster(true);break;
		}
		JuicePrefs.SetDouble("Monster_maxhealth", maxHealth);
		JuicePrefs.SetDouble("Monster_currentMaxHealth", currentMaxHealth);
		JuicePrefs.SetDouble("Monster_Health", Health);
		JuicePrefs.SetInt("Monster_level", level);
		JuicePrefs.SetTimeStamp();
	}
	public void Hit(){
		if(monsterable && monsterhitable){
			if(CurrentMonsterAnim!=null)
			{
				Monsters[currentMonster].GetComponent<AudioSource>().Play ();
				CurrentMonsterAnim.SetTrigger("hit");
			}
		}
	}
	public void Die(){
		poof.Play();
		monsterable = false;
		Invoke("setablemonster", 0.5f);
		Invoke("sethitable", 0.9f);
		Monsters[currentMonster].SetActive(false);
	}
	void setablemonster (){
		monsterable = true;
		Monsters[currentMonster].SetActive(true);
	}
	void sethitable (){
		monsterhitable = true;
	}
}
