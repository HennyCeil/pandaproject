﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class MagicFlowerSystemController : MonoBehaviour {
	public static MagicFlowerSystemController instance;
	private readonly object syncLock = new object();
	private Queue<int> MagicFlowerQueue;
	private int magicflowers;
	private int b;
	public Text counter;
	public Animator FlowerCounterAnimator;
	private bool showingcounter = false;
	private float secstohide;
	// Use this for initialization
	void Awake(){
		instance = this;
		magicflowers = JuicePrefs.GetInt("Flowers_amount",0);
	}
	void Start () {
		MagicFlowerQueue = new Queue<int>();

	}

	// Update is called once per frame
	void Update () {
		while(MagicFlowerQueue.Count>0){
			lock(syncLock){
				b = MagicFlowerQueue.Dequeue();
				magicflowers+=b;
				b=0;
				JuicePrefs.SetInt("Flowers_amount",magicflowers);
				if(!showingcounter){
					FlowerCounterAnimator.SetTrigger("show");
				}
				showingcounter = true;
				secstohide = 3f;
			}
		}
		if(showingcounter){
			secstohide-=Time.deltaTime;
			if(secstohide <=0)
				HideBeanCounter();
		}

		counter.text = magicflowers.ToString();
	}
	void HideBeanCounter(){
		FlowerCounterAnimator.SetTrigger("hide");
		showingcounter = false;
	}
	public double GetTotalFlowers(){
		return magicflowers;
	}
	public void AddFlowers(int v){
		lock(syncLock){
			MagicFlowerQueue.Enqueue(v);
		}
	}
}
