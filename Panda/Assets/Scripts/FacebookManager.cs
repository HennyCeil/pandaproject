using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FacebookManager : MonoBehaviour
{
	#region Variables
	public static FacebookManager instance;
	private string status = "Ready";
	private string lastResponse = "";
	private Texture2D lastResponseTexture;
	
	public string username = "";
	public string userFirstName = "";
	public string userFullName = "";
	public string userID = "";
	
	public bool hasTriedFBShare = false;
	

	#endregion
	
	#region FB.Init() example
	
	public bool isInit = false;
	
	public void CallFBInit()
	{
		FB.Init(OnInitComplete, OnHideUnity);
	}
	
	public void CallFBInit(Facebook.InitDelegate callback)
	{
		FB.Init(callback, OnHideUnity);
	}
	
	
	private void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		isInit = true;
		CallFBPublishInstall();
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}
	
	#endregion
	
	#region FB.Login() example
	
	private void CallFBLogin(Facebook.FacebookDelegate callback)
	{
		FB.Login("email,publish_actions", callback);
	}
	
	private void CallFBLogin()
	{
		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void LoginCallback(FBResult result)
	{
		if (result.Error != null){
			lastResponse = "Error Response:\n" + result.Error;
		}
		else if (!FB.IsLoggedIn)
		{
			lastResponse = "Login cancelled by Player";
		}else{
			//SocialFeatureManager.UnlockAchievement(LinesAchievement.achievementLineOnFacebook);
		}
	}
	
	private void CallFBLogout()
	{
		FB.Logout();
	}
	#endregion
	
	#region FB.PublishInstall() example
	
	private void CallFBPublishInstall()
	{
		FB.ActivateApp();
	}

	
	#endregion
	


	
	#region FB.Feed() example
	
	public string FeedToId = "";
	public string FeedLink = "";
	public string FeedLinkName = "";
	public string FeedLinkCaption = "";
	public string FeedLinkDescription = "";
	public string FeedPicture = "";
	public string FeedMediaSource = "";
	public string FeedActionName = "";
	public string FeedActionLink = "";
	public string FeedReference = "";
	public bool IncludeFeedProperties = false;
	private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();
	
	private void CallFBFeed()
	{
		Dictionary<string, string[]> feedProperties = null;
		if (IncludeFeedProperties)
		{
			feedProperties = FeedProperties;
		}
		FB.Feed(
			toId: FeedToId,
			link: FeedLink,
			linkName: FeedLinkName,
			linkCaption: FeedLinkCaption,
			linkDescription: FeedLinkDescription,
			picture: FeedPicture,
			mediaSource: FeedMediaSource,
			actionName: FeedActionName,
			actionLink: FeedActionLink,
			reference: FeedReference,
			properties: feedProperties,
			callback: Callback
			);
	}
	
	#endregion
	
	#region FB.Canvas.Pay() example
	
	public string PayProduct = "";
	
	private void CallFBPay()
	{
		FB.Canvas.Pay(PayProduct);
	}
	
	#endregion
	
	#region FB.API() example
	
	public string ApiQuery = "";
	
	private void CallFBAPI()
	{
		FB.API(ApiQuery, Facebook.HttpMethod.GET, Callback);
	}
	
	#endregion
	
	#region FB.GetDeepLink() example
	
	private void CallFBGetDeepLink()
	{
		FB.GetDeepLink(Callback);
	}
	
	#endregion
	
	#region FB.AppEvent.LogEvent example
	
	public float PlayerLevel = 1.0f;
	
	public void CallAppEventLogEvent()
	{
		var parameters = new Dictionary<string, object>();
		parameters[Facebook.FBAppEventParameterName.Level] = "Player Level";
		FB.AppEvents.LogEvent(Facebook.FBAppEventName.AchievedLevel, PlayerLevel, parameters);
		PlayerLevel++;
	}
	
	#endregion


	
	#region Support
	
	public void Logout()
	{
		CallFBLogout();
		status = "Logout called";
	}
	
	
	public void Login()
	{
		CallFBLogin();
		status = "Login called";
	}
	
	public void Login(Facebook.FacebookDelegate callback)
	{
		CallFBLogin(callback);
		status = "Login called";
	}
	
	void Callback(FBResult result)
	{
		lastResponseTexture = null;
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!ApiQuery.Contains("/picture"))
			lastResponse = "Success Response:\n" + result.Text;
		else
		{
			lastResponseTexture = result.Texture;
			lastResponse = "Success Response:\n";
		}
	}
	
	private IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();
		
		var width = Screen.width;
		var height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		byte[] screenshot = tex.EncodeToPNG();
		
		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
		wwwForm.AddField("message", "herp derp.  I did a thing!  Did I do this right?");
		
		FB.API("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
	}
	
	/*private bool Button(string label)
    {
        return GUILayout.Button(
          label,
          GUILayout.MinHeight(buttonHeight),
          GUILayout.MaxWidth(mainWindowWidth)
        );
    }*/

	private void LabelAndTextField(string label, ref string text)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(label, GUILayout.MaxWidth(150));
		text = GUILayout.TextField(text);
		GUILayout.EndHorizontal();
	}
	
	private bool IsHorizontalLayout()
	{
		#if UNITY_IOS || UNITY_ANDROID
		return Screen.orientation == ScreenOrientation.Landscape;
		#else
		return true;
		#endif
	}
	
	#endregion
	
	#region Core
	
	public string facebookQuery = "/me?fields=id,name,first_name"; //,username,friends.limit(100).fields(first_name,id)";
	public Dictionary<string, string> profile;
	
	
	void Awake()
	{
		DontDestroyOnLoad(this);
		instance = this;
		if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.WindowsEditor && Application.platform != RuntimePlatform.OSXEditor)
			Destroy(gameObject);
		

		if (!isInit)
        {
            CallFBInit();
        }
	}
	
	void Start()
	{
		CallFBInit();
	}
	
	void Update()
	{
		
	}
	
	
	#endregion
	
	
	public void UpdateLocalUserInfo()
	{
		FB.API("/me?fields=id,first_name,gender,age_range", Facebook.HttpMethod.GET, UpdateLocalUserInfoCallback);  
	}
	
	void UpdateLocalUserInfoCallback(FBResult result)
	{
		if (result.Error != null)
		{
			Debug.LogError(result.Error);
			FB.API("/me?fields=id,first_name,gender,age_range", Facebook.HttpMethod.GET, UpdateLocalUserInfoCallback);
			return;
		}

		var n = SimpleJSON.JSON.Parse(result.Text);
		var i = n["age_range"]["min"].AsInt;

		JuicePrefs.SetString("FB_GENDER", n["gender"]);
		JuicePrefs.SetInt("FB_MIN_AGE", i);
	}
	
	public void FBShare(Facebook.FacebookDelegate callback = null)
	{
		string[] title = {"New Mobile Game!", "Must Be 21+ to PLAY!"};
		FBShare(title[UnityEngine.Random.Range(0, 2)], "www.juiceglobal.net/", 
		        "WARNING: The most fun and addictive arcade game is now here! Can you beat my Score?\n[PLAY NOW]", 
		        callback);
	}
	
	public void FBShare(string title, string caption, string body, Facebook.FacebookDelegate callback = null)
	{
		string feedPicture = "https://lh5.googleusercontent.com/-Juz5bJBwWow/VMZtUAeVOnI/AAAAAAAAEDI/4HyiQJ44XKE/w940-h788-no/Facebook%2Bimage.png";
		string feedLink = "http://www.juiceglobal.net/";

		
		if (callback == null)
			FB.Feed(
				linkName: title,
				linkCaption: caption,
				picture: feedPicture,
				link: feedLink,
				linkDescription: body
				);
		else
			FB.Feed(
				linkName: title,
				linkCaption: caption,
				picture: feedPicture,
				link: feedLink,
				linkDescription: body,
				callback: callback
				);
	}
	
}
