﻿using UnityEngine;
using System.Collections;

public class BackgroundChanger : MonoBehaviour {
	public static BackgroundChanger instance;
	public GameObject[] BackgroundsGO;
	private int count = 0;
	void Awake(){
		instance = this;
		count = JuicePrefs.GetInt("background_count", 0);
	}
	void Start(){
		for(int i = 0; i<BackgroundsGO.Length; i++)
			BackgroundsGO[i].SetActive(false);
		BackgroundsGO[count].SetActive(true);
	}
	IEnumerator ChangeNextBack(){
		yield return new WaitForSeconds(1);
		count++;
		for(int i = 0; i<BackgroundsGO.Length; i++)
			BackgroundsGO[i].SetActive(false);
		if(count>=BackgroundsGO.Length){
			count = 0;
		}
		BackgroundsGO[count].SetActive(true);
		JuicePrefs.SetInt("background_count", count);
	}
	public void ChangeNext(){
		StartCoroutine(ChangeNextBack());
	}
}
