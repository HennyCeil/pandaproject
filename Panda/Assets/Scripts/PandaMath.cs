﻿using UnityEngine;
using System.Collections;

public class PandaMath : MonoBehaviour {
	public static double UpgradeCostBase = 1.092d;
	public static double UpgradeDmgBase = 1.096d;
	public static double monsterHPfactor1 = 0.98d;
	public static double monsterHPfactor2 = 1.05d;
	public static double TotalFruitDiscount = 0.0d;
	public static double TotalAllDamage = 0.0d;
	public static double initialHPMonster = 26.9d;
	public static double secondHPMonster = 43.9;
	public static double BossMultiplier = 7;
	public static double MiniBossMultiplier = 3;
	public static float CageChace = 0.01f;
	public static float HelperGlobalSpeed = 1f;
	public static float BaseCritChance = 5f;
	public static int TapCritDamageMin = 2;
	public static int TapCritDamageMax = 6;
	public static int BaseCritDamage = 1;

	
	public static double PandaDamageBaseCost = 5;
	public static double PandaDamageBase = 1d;
	public static double[] HelpersBaseCost = {110, 4950, 172155, 6.85947E+12, 2.28354E+18, 5.52315E+24, 4.68851E+31, 3.14159E+40, 8.76543E+55, 9.87654E+70};
	public static double[] HelpersBaseDamage = {0.049f,0.0765f,0.08f,0.1f,0.13f,0.17f,0.22f,0.35f,0.5f};
	public static int[] FruitsBaseCost = {5,20,75,130,300,500,1200,1500,5000};
	public static int[] FruitsIncrementBaseCost = {5,20,7,10,12,5,15,100,150};
	public static double[] FruitsIncrementMultiplierCost = {1.75d,1.5d,1.2d,1d,1.1d,1.75d,1.25d,1.6d,1.1d};
	public static int[] FruitsLevelCap = {0,41,9,0,0,9,0,5,10};
	public static double[] FruitsIncrementValue = {2d,5d,5d,5d,15d,5d,2d,100d,5d};
	public static double[] FruitsBaseValue = {10d,5d,10d,20d,50d,10d,10d,100d,25d};

	// Use this for initialization.
	public static double GetUpgradeCost(double Level, double basecost)
	{
		double cost = 0.0d;
		cost = basecost * System.Math.Pow(UpgradeCostBase, Level)*IncreasedEfectiveness(Level);
		cost = cost * (1.0 + TotalFruitDiscount);
		cost = cost + 1;
		return System.Math.Ceiling(cost);
	}
	public static double GetUpgradeDamage(int Level, double basedamage){
		double dmg = 0.0d;
		dmg = basedamage * System.Math.Pow(UpgradeDmgBase, Level)*DecreasedEfectiveness(Level);
		dmg = dmg * (1.0 + TotalAllDamage);
		dmg = dmg+1;
		return System.Math.Floor(dmg);
	}

	public static double GetFibonacciForLevel(int level, double factor1, double factor2, double initialvalue, double secondvalue){
		double ret = 0.0d;
		double n = level;
		double x = initialvalue;
		double y = secondvalue;
		double a = factor1;
		double b = factor2;
		double sqrtaa4b = System.Math.Sqrt(a*a*4*b);
		ret = ((1.0d/sqrtaa4b)*(System.Math.Pow(2,(-n-1))))*((System.Math.Pow((a-sqrtaa4b),n) * ((a*x)-(2*y))) + ((System.Math.Pow((a+sqrtaa4b),n)) * ((2*y)-(a*x))) + ((x*sqrtaa4b)*System.Math.Pow((a-sqrtaa4b),n)) + ((x*sqrtaa4b)*System.Math.Pow((a+sqrtaa4b),n)));
		return ret;
	}
	public static double GetCumulativeUpgradeCost(int levelFRom, int levelTo, double basecost){
		double ret = 0.0d;
		for(int i = levelFRom; i<levelTo; i++){
			ret+=GetUpgradeCost(i, basecost);
		}
		return ret;
	}
	public static string SecsToString(float s){
		int minutes = Mathf.FloorToInt(s / 60F);
		int seconds = Mathf.FloorToInt(s - minutes * 60);
		
		return string.Format("{0:00}:{1:00}", minutes, seconds);
	}
	public static float CurrentCritChance(){
		float crit;
		crit = BaseCritChance + SkillsManager.instance.critchanceskill;
		crit += HelpersController.instance.GetCritChanceBonus();
		if(crit>100)
			crit = 100;
		return crit;
	}
	public static float GetTapDamageBonus(){
		float ret;
		ret = SkillsManager.instance.wrathdamaebonus;
		ret *= (float)HelpersController.instance.GetTapDamageBonus();
		return ret;
	}
	public static double GetHelpersDamageBonus(){
		return HelpersController.instance.GetHelpersDamageBonus();
	}
	public static double GetGoldBonus(bool boss, bool cage){
		if(boss)
			return 2;
		if(cage)
			return 2;
		return 1;
	}
	public static float CurrentCritDamage(){
		return (float)(BaseCritDamage + HelpersController.instance.GetCritDamageBonus());
	}
	public static float CurrentCritDamageOnBoss(){
		return (float)HelpersController.instance.GetCritDamageBossBonus();
	}
	public static double CurrentDamageOnBoss(){
		return HelpersController.instance.GetDamageOnBoss();
	}
	public static double DamageAllBonus(){
		return HelpersController.instance.GetDamageAllBonus();
	}
	public static double IncreasedEfectiveness(double x){
		return((((x*x)/(System.Math.Sqrt(25000.0d+ (x*x*x))))+1d)+x/10.0d);
	}

	public static double DecreasedEfectiveness(double x){
		double r = System.Math.Pow((1d/System.Math.Log10(x+10d)), 0.95d);
		r+=0.33d;
		if(r>1)
			r=1;
		return r;
	}
	public static void SetDouble(string key, double value)
	{
		PlayerPrefs.SetString(key, DoubleToString(value));
	}
	public static double GetDouble(string key, double defaultValue)
	{
		string defaultVal = DoubleToString(defaultValue);
		return StringToDouble(PlayerPrefs.GetString(key, defaultVal));
	}
	public static double GetDouble(string key)
	{
		return GetDouble(key, 0d);
	}
	
	private static string DoubleToString(double target)
	{
		return target.ToString("R");
	}
	private static double StringToDouble(string target)
	{
		if (string.IsNullOrEmpty(target))
			return 0d;
		
		return double.Parse(target);
	}

	public static double GetGoldAllBonus(){
		double value = 1;
		value += HelpersController.instance.GetGoldAllBonus();
		return  value;
	}
	public static double GetGoldBossBonus(){
		double value = 1;
		value += HelpersController.instance.GetGoldBossBonus();
		return  value;
	}
	public static double GetGoldCageBonus(){
		double value = 1;
		value += HelpersController.instance.GetGoldCageBonus();
		return  value;
	}
	public static double GetGoldMonstereBonus(){
		double value = 1;
		value += HelpersController.instance.GetGoldMonstereBonus();
		return  value;
	}
}