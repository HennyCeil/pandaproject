//
//  iOSBridge.h
//  iOSBridge
//
//  Created by Supersonic.
//  Copyright (c) 2015 Supersonic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Supersonic/Supersonic.h>
#import <SUpersonic/SUSupersonicAdsConfiguration.h>


@interface iOSBridge : NSObject<SupersonicRVDelegate, SupersonicISDelegate, SupersonicOWDelegate>

+ (iOSBridge*) start;
- (void)setAge:(int)age;
- (void)setGender:(NSString *)gender;

/*-----------------------------------------------*/
// Rewarded Video
/*-----------------------------------------------*/

- (void)initRewardedVideoWithAppKey:(NSString *)appKey withUserId:(NSString*)userId;
- (void)showRewardedVideo;
- (BOOL)isRewardedVideoAvailable;

/*-----------------------------------------------*/
// Interstitial
/*-----------------------------------------------*/
- (void)initInterstitialWithAppKey:(NSString *)appKey withUserId:(NSString*)userId;
- (void)showInterstitial;
- (BOOL)isInterstitialAdAvailable;

/*-----------------------------------------------*/
// Offerwall
/*-----------------------------------------------*/
- (void)initOfferwallWithAppKey:(NSString *)appKey withUserId:(NSString*)userId;
- (void)showOfferwall;
- (void)getOfferwallCredits;
- (BOOL)isOfferwallAvailable;

@end