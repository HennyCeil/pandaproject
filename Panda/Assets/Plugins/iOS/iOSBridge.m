//
//  iOSBridge.m
//  iOSBridge
//
//  Created by Supersonic.
//  Copyright (c) 2015 Supersonic. All rights reserved.
//

#import "iOSBridge.h"




#ifdef __cplusplus
extern "C" {
#endif

extern void UnitySendMessage( const char * className, const char * methodName, const char * param );


#ifdef __cplusplus
}
#endif

    
    
    
@implementation iOSBridge

char* const SUPERSONIC_EVENTS = "SupersonicEvents";


+ (iOSBridge *)start{
    static iOSBridge *instance;
    static dispatch_once_t onceToken;
    dispatch_once( &onceToken,
                  ^{
                      instance = [[iOSBridge alloc] init];
                  });
    
    return instance;
}

- (instancetype)init{
    self = [super init];
    if(self){
        [Supersonic sharedInstance];
        [SUSupersonicAdsConfiguration getConfiguration].platform = @"Unity";
    }
    
    return self;
}



- (NSString *)getJsonFromDic:(NSDictionary *)dict{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return @"";
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
}



-(void)setAge:(int)age{
    [[Supersonic sharedInstance] setAge:age];
}


-(void)setGender:(NSString *)gender{
    if([gender caseInsensitiveCompare:@"male"] == NSOrderedSame)
        [[Supersonic sharedInstance] setGender:SUPERSONIC_USER_MALE];
    
    else if([gender caseInsensitiveCompare:@"female"] == NSOrderedSame)
        [[Supersonic sharedInstance] setGender:SUPERSONIC_USER_FEMALE];
    
    else if([gender caseInsensitiveCompare:@"unknown"] == NSOrderedSame)
        [[Supersonic sharedInstance] setGender:SUPERSONIC_USER_UNKNOWN];
}


#pragma mark RewardedVideo API

- (void)initRewardedVideoWithAppKey:(NSString *)appKey withUserId:(NSString*)userId{
    [[Supersonic sharedInstance] setRVDelegate:self];
    [[Supersonic sharedInstance] initRVWithAppKey:appKey withUserId:userId];
}

-(void)showRewardedVideo{
    [[Supersonic sharedInstance] showRV];
}

-(BOOL)isRewardedVideoAvailable{
    return [[Supersonic sharedInstance] isAdAvailable];
}


#pragma mark RewardedVideoDelegate

-(void)supersonicRVInitSuccess {
    UnitySendMessage(SUPERSONIC_EVENTS, "onRewardedVideoInitSuccess", "");
}

- (void)supersonicRVInitFailedWithError:(NSError *)error{
    UnitySendMessage(SUPERSONIC_EVENTS, "onRewardedVideoInitFail", [self parseErrorToEvent:error]);
}

-(void)supersonicRVAdAvailabilityChanged:(BOOL)hasAvailableAds{
    UnitySendMessage(SUPERSONIC_EVENTS, "onVideoAvailabilityChanged", (hasAvailableAds) ? "true" : "false");
}

-(void)supersonicRVAdOpened{
    UnitySendMessage(SUPERSONIC_EVENTS, "onRewardedVideoAdOpened", "");
}

-(void)supersonicRVAdStarted{
    UnitySendMessage(SUPERSONIC_EVENTS, "onVideoStart", "");
}

-(void)supersonicRVAdEnded{
    UnitySendMessage(SUPERSONIC_EVENTS, "onVideoEnd", "");
}

-(void)supersonicRVAdClosed{
    UnitySendMessage(SUPERSONIC_EVENTS, "onRewardedVideoAdClosed", "");
}

-(void)supersonicRVAdRewarded:(NSInteger)amount{
    UnitySendMessage(SUPERSONIC_EVENTS, "onRewardedVideoAdRewarded", [NSString stringWithFormat:@"%ld", amount].UTF8String);
}

- (void)supersonicRVAdFailedWithError:(NSError *)error{
    
}






#pragma mark Interstitial API

-(void)initInterstitialWithAppKey:(NSString *)appKey withUserId:(NSString*)userId{
    [[Supersonic sharedInstance] setISDelegate:self];
   [[Supersonic sharedInstance] initISWithAppKey:appKey withUserId:userId];
}

-(void)showInterstitial{
    [[Supersonic sharedInstance] showIS];
}

-(BOOL)isInterstitialAdAvailable{
    return [[Supersonic sharedInstance] isISAdAvailable];
}






#pragma mark InterstitialDelegate

-(void)supersonicISInitSuccess{
    UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialInitSuccess", "");
}

-(void)supersonicISInitFailedWithError:(NSError *)error{
    if (error)
        UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialInitFail", [self parseErrorToEvent:error]);
    else
        UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialInitFail", "");
}

-(void)supersonicISShowSuccess{
    UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialShowSuccess", "");
}

-(void)supersonicISShowFailWithError:(NSError *)error{
    if (error)
        UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialShowFail", [self parseErrorToEvent:error]);
    else
        UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialShowFail","");
}

-(void)supersonicISAdAvailable:(BOOL)available{
    UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialAvailability", (available) ? "true" : "false");
}

-(void)supersonicISAdClicked{
    UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialAdClicked", "");
}

-(void)supersonicISAdClosed{
    UnitySendMessage(SUPERSONIC_EVENTS, "onInterstitialAdClosed", "");
}



//public boolean onOfferwallAdCredited(int credits, int totalCredits, boolean totalCreditsFlag);
//public void onGetOfferwallCreditsFail(SupersonicError supersonicError);


#pragma mark Offerwall API

-(void)initOfferwallWithAppKey:(NSString *)appKey withUserId:(NSString*)userId{
    [[Supersonic sharedInstance] setOWDelegate:self];
    [[Supersonic sharedInstance] initOWWithAppKey:appKey withUserId:userId];
}

-(void)showOfferwall{
    [[Supersonic sharedInstance] showOW];
}

-(void)getOfferwallCredits{
    [[Supersonic sharedInstance] getOWCredits];
}

-(BOOL)isOfferwallAvailable{
    return [[Supersonic sharedInstance] isOWAvailable];
}



#pragma mark OfferwallDelegate

-(void)supersonicOWInitSuccess{
    UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallInitSuccess", "");
}

-(void)supersonicOWShowSuccess{
    UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallOpened", "");
}

-(void)supersonicOWInitFailedWithError:(NSError *)error{
    if (error)
        UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallInitFail", [self parseErrorToEvent:error]);
    else
        UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallInitFail", "");
}

-(void)supersonicOWShowFailedWithError:(NSError *)error{
    if (error)
        UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallShowFail", [self parseErrorToEvent:error]);
    else
        UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallShowFail", "");
}

-(void)supersonicOWAdClosed{
    UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallClosed", "");
}

- (BOOL)supersonicOWDidReceiveCredit:(NSDictionary *)creditInfo{
    if(creditInfo)
        UnitySendMessage(SUPERSONIC_EVENTS, "onOfferwallAdCredited", [self getJsonFromDic:creditInfo].UTF8String);
    
    return YES;
}

-(void)supersonicOWFailGettingCreditWithError:(NSError *)error{
    if (error)
        UnitySendMessage(SUPERSONIC_EVENTS, "onGetOfferwallCreditsFail", [self parseErrorToEvent:error]);
    else
        UnitySendMessage(SUPERSONIC_EVENTS, "onGetOfferwallCreditsFail", "");
}



-(const char*)parseErrorToEvent:(NSError *)error{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (error){
        NSString* codeStr =  [NSString stringWithFormat:@"%d", [error code]];
        [dict setObject:[error localizedDescription] forKey:@"error_description"];
        [dict setObject:codeStr forKey:@"error_code"];
    }
    const char* res = [self getJsonFromDic:dict].UTF8String;
    return res;
}


#pragma mark C Section


#ifdef __cplusplus
extern "C" {
#endif

#define ParseNSStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]

    void CFStart(){
        [iOSBridge start];
    }

    void CFSetAge(int age){
        [[iOSBridge start] setAge:age];
    }
    
    void CFSetGender(const char* gender){
        [[iOSBridge start] setGender:ParseNSStringParam(gender)];
    }
    
    
    
    void CFInitRewardedVideo(const char* appKey, const char* userId){
        [[iOSBridge start] initRewardedVideoWithAppKey:ParseNSStringParam(appKey) withUserId:ParseNSStringParam(userId)];

    }


    void CFShowRewardedVideo(){
        [[iOSBridge start] showRewardedVideo];
    }


    bool CFIsRewardedVideoAvailable(){
        return [[iOSBridge start] isRewardedVideoAvailable];
    }




    void CFInitInterstitial(const char* appKey,const char* userId){
        [[iOSBridge start] initInterstitialWithAppKey:ParseNSStringParam(appKey) withUserId:ParseNSStringParam(userId)];
    }

    void CFShowInterstitial(){
        [[iOSBridge start] showInterstitial];
    }

    bool CFIsInterstitialAdAvailable(){
        return [[iOSBridge start] isInterstitialAdAvailable];
    }


    

    void CFInitOfferwall(const char* appKey,const char* userId){
        [[iOSBridge start] initOfferwallWithAppKey:ParseNSStringParam(appKey) withUserId:ParseNSStringParam(userId)];
    }

    void CFShowOfferwall(){
        [[iOSBridge start] showOfferwall];
    }

    void CFGetOfferwallCredits(){
        [[iOSBridge start] getOfferwallCredits];
    }
    
    bool CFIsOfferwallAvailable(){
        return [[iOSBridge start] isOfferwallAvailable];
    }
    




#ifdef __cplusplus
}
#endif























@end