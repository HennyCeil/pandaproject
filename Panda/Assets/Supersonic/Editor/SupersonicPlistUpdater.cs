﻿using UnityEngine;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using UnityEditor;

namespace Supersonic.UnityEditor
{
	
	public class PlistUpdater
	{
		
		private static XmlNode GetPlistDictionary(XmlDocument xmlDoc)
		{
			var currentChild = xmlDoc.FirstChild;
			while(currentChild != null)
			{
				if(currentChild.Name.Equals("plist") && currentChild.ChildNodes.Count == 1)
				{
					var dict = currentChild.FirstChild;
					if(dict.Name.Equals("dict"))
						return dict;
				}
				currentChild = currentChild.NextSibling;
			}
			return null;
		}
		
		private static XmlElement AddChildElement(XmlDocument xmlDoc, XmlNode parentNode, string elementName, string childText=null)
		{
			var element = xmlDoc.CreateElement(elementName);
			if(!string.IsNullOrEmpty(childText))
				element.InnerText = childText;
			
			parentNode.AppendChild(element);
			return element;
		}
		
		private static XmlNode HasKey(XmlNode xmlDict, string key)
		{
			var currentChild = xmlDict.FirstChild;
			while(currentChild != null)
			{
				if(currentChild.Name.Equals("key") && currentChild.InnerText.Equals(key))
					return currentChild;
				currentChild = currentChild.NextSibling;
			}
			return null;
		}
		
		private static XmlNode GetNetworkNode(XmlNode xmlDict, XmlDocument xmlDoc)
		{
			var name = HasKey(xmlDoc.DocumentElement, "name").NextSibling.InnerText;
			var currentChild = xmlDict.FirstChild;
			while(currentChild != null)
			{
				if(currentChild.InnerText.Contains(name))
					return currentChild;
				currentChild = currentChild.NextSibling;
			}
			return null;
		}
		
		
		public static void Update(string path, string xmlNodeString)
		{
			const string fileName = "Info.plist";
			string fullPath = Path.Combine(path, fileName);
			var plistAsXml = new XmlDocument();
			plistAsXml.Load(fullPath);
			
			var plistDict = GetPlistDictionary(plistAsXml);
			if(plistDict == null)
			{
				Debug.LogError("Error parsing existing plist " + fullPath);
				return;
			}
			
			var config = new XmlDocument();
			config.LoadXml(xmlNodeString);
			
			if (config.FirstChild.Name.Equals("root"))
			{
				foreach (XmlNode node in config.FirstChild.ChildNodes)	{
					XmlNode imported = plistAsXml.ImportNode(node, true);
					plistDict.AppendChild(imported);
				}
			} else {
				
				
				XmlNode allProvidersConfig = HasKey(plistDict, "SupersonicConfig");			
				
				if(allProvidersConfig == null) {
					allProvidersConfig = AddChildElement(plistAsXml, plistDict, "key", "SupersonicConfig");
					
					//AddChildElement(xmlDoc, dict, "array");
				} else {
					allProvidersConfig.RemoveAll();
				}

				
				
				//XmlNode array = networks.NextSibling;
				
				/*var networkNode = GetNetworkNode(array, config);
				if ( networkNode != null) {
					array.RemoveChild(networkNode);
				}*/
				
				bool addAppLovinSdkKey = false;
				string appLovinSdkKeyValueStr = "[Your_AppLovin_Sdk_Key]";
				string appLovinSdkKeyKeyStr = "AppLovinSdkKey";
				
				var providerConfig = AddChildElement(plistAsXml, plistDict, "dict");
				foreach (XmlNode node in config.DocumentElement.ChildNodes)
				{
					XmlNode imported = plistAsXml.ImportNode(node, true);
					if (imported != null && imported.FirstChild != null){
						if(imported.FirstChild.Value != null && imported.FirstChild.Value.Equals("AppLovin")){
							addAppLovinSdkKey = true;
						}
						else if(imported.FirstChild.InnerText.Equals(appLovinSdkKeyKeyStr)){
							appLovinSdkKeyValueStr = imported.FirstChild.NextSibling.InnerText;
						}
					}
					providerConfig.AppendChild(imported);
				}
				
				if (addAppLovinSdkKey){
					XmlNode appLovinKey = HasKey(plistDict, appLovinSdkKeyKeyStr);		
					if(appLovinKey == null) {
						appLovinKey = AddChildElement(plistAsXml, plistDict, "key", appLovinSdkKeyKeyStr);
						AddChildElement(plistAsXml, plistDict, "string", appLovinSdkKeyValueStr);
					}
				}
				
			}
			
			//Strip whitespace from empty strings
			Regex nonwhite = new Regex("\\S");
			XmlNodeList elemList = plistAsXml.GetElementsByTagName("string");
			
			for (int i=0; i < elemList.Count; i++) {   
				if (!nonwhite.IsMatch(elemList[i].InnerText)) {
					elemList[i].InnerText = "";
				}
			} 
			
			XmlWriterSettings settings = new XmlWriterSettings {
				Indent = true,
				IndentChars = "\t",
				NewLineHandling = NewLineHandling.None
			};
			
			XmlWriter xmlwriter = XmlWriter.Create(fullPath, settings);
			plistAsXml.Save(xmlwriter);
			xmlwriter.Close();	
			
			//the xml writer barfs writing out part of the plist header.
			//so we replace the part that it wrote incorrectly here
			var reader = new StreamReader(fullPath);
			string textPlist = reader.ReadToEnd();
			reader.Close();
			
			int fixupStart = textPlist.IndexOf("<!DOCTYPE plist PUBLIC", System.StringComparison.Ordinal);
			if(fixupStart <= 0)
				return;
			int fixupEnd = textPlist.IndexOf('>', fixupStart);
			if(fixupEnd <= 0)
				return;
			
			string fixedPlist = textPlist.Substring(0, fixupStart);
			fixedPlist += "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
			fixedPlist += textPlist.Substring(fixupEnd+1);
			
			var writer = new StreamWriter(fullPath, false);
			writer.Write(fixedPlist);
			writer.Close();
		}
	}
}
